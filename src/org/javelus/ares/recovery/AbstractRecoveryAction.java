package org.javelus.ares.recovery;

import gov.nasa.jpf.vm.ThreadInfo;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

public abstract class AbstractRecoveryAction implements RecoveryAction {
    protected ThreadInfo ti;

    protected int failureDepth;
    protected int recoveryDepth;
    protected int expectedStoppingDepth;

    public AbstractRecoveryAction(ThreadInfo ti, int failureDepth, int recoveryDepth, int expectedFinalDepth) {
        this.ti = ti;
        this.failureDepth = failureDepth;
        this.recoveryDepth = recoveryDepth;
        this.expectedStoppingDepth = expectedFinalDepth >= 0 ? expectedFinalDepth : 0;
    }

    @Override
    public ThreadInfo getRecoveryThread() {
        return ti;
    }

    public int getFailureDepth() {
        return failureDepth;
    }

    public int getRecoveryDepth() {
        return recoveryDepth;
    }

    public int getExpectedStoppingDepth() {
        return expectedStoppingDepth;
    }


    public void toString(PrintWriter pw) {
        pw.printf("%s: fd=%d, rd=%d, esd=%d",
                getClass().getSimpleName(),
                failureDepth,
                recoveryDepth,
                expectedStoppingDepth);
    }

    public String toString() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(baos);

        toString(pw);

        pw.close();

        return baos.toString();
    }

}
