package org.javelus.ares.recovery;

public class RecoveryResult {

    private RecoveryAction action;

    private ResultType resultType;

    private int actualStoppingDepth;

    private int executedInstructions;

    public RecoveryResult(RecoveryAction action, 
            ResultType resultType, int actualFinalDepth,
            int executedInstructions) {
        this.action = action;
        this.resultType = resultType;
        this.actualStoppingDepth = actualFinalDepth;
        this.executedInstructions = executedInstructions;
    }

    public ResultType getResultType() {
        return resultType;
    }

    public RecoveryAction getAction() {
        return action;
    }

    public int getActualStoppingDepth() {
        return actualStoppingDepth;
    }

    public int getFailureDepth() {
        return action.getFailureDepth();
    }

    public int getRecoveryDepth() {
        return action.getRecoveryDepth();
    }

    public int getDroppedLength() {
        return action.getFailureDepth() - action.getRecoveryDepth();
    }

    public int getExecisedLength() {
        return action.getRecoveryDepth() - getActualStoppingDepth();
    }

    public int getExecutedInstructions() {
        return executedInstructions;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(action);
        sb.append(", asd=");
        sb.append(actualStoppingDepth);
        sb.append(", r=");
        sb.append(resultType);
        sb.append(", c=");
        sb.append(getExecisedLength());
        sb.append(", s=");
        sb.append(executedInstructions);
        return sb.toString();
    }
}
