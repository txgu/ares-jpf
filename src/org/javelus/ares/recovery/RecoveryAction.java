package org.javelus.ares.recovery;

import gov.nasa.jpf.vm.ThreadInfo;

import java.io.PrintWriter;

public interface RecoveryAction {
    ThreadInfo getRecoveryThread();

    /**
     * Depth where a failure is detected
     * @return
     */
    int getFailureDepth();

    /**
     * Depth where we begin to recover
     * Dropped Frames = FailureDepth - RecoveryDepth
     * @return
     */
    int getRecoveryDepth();

    /**
     * The expected depth the recovery should reach.
     * Note all depth is calculated on the original failure thread.
     * @return
     */
    int getExpectedStoppingDepth();

    boolean recover();

    void toString(PrintWriter pw);
}