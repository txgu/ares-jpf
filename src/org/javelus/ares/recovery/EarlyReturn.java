package org.javelus.ares.recovery;

import gov.nasa.jpf.jvm.RecoveryHelper;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.bytecode.InvokeInstruction;

import java.io.PrintWriter;

public class EarlyReturn extends AbstractRecoveryAction {

    private String returnType;

    public EarlyReturn (ThreadInfo ti, int recoveryDepth) {
        this(ti, recoveryDepth, recoveryDepth - 1);
    }

    public EarlyReturn (ThreadInfo ti, int recoveryDepth, int expectedFinalDepth) {
        super(ti, ti.getTopFrame().getDepth(), recoveryDepth, expectedFinalDepth);
        seekReturnType(ti, recoveryDepth);
    }

    private void seekReturnType(ThreadInfo ti, int recoveryDepth) {
        StackFrame frame = ti.getTopFrame();

        if (frame.getDepth() < recoveryDepth) {
            throw new RuntimeException();
        }

        for (; (frame != null) && (frame.getDepth() > recoveryDepth); frame = frame.getPrevious()) {
        }

        // XXX frame should not be null
        // Cause we make an 
        InvokeInstruction ii = (InvokeInstruction) frame.getPC();
        String cname = ii.getInvokedMethodClassName();
        String mname = ii.getInvokedMethodName();

        MethodInfo mi = RecoveryHelper.getStaticTarget(frame.getMethodInfo(), cname, mname);
        
        returnType = mi.getReturnType();
    }
    
    @Override
    public boolean recover() {
        StackFrame frame = ti.getTopFrame();

        if (frame.getDepth() < recoveryDepth) {
            throw new RuntimeException();
        }

        for (; (frame != null) && (frame.getDepth() > recoveryDepth); frame = frame.getPrevious()) {
            ti.leave();
            ti.popFrame();
        }

        // XXX frame should not be null
        // Cause we make an 
        InvokeInstruction ii = (InvokeInstruction) frame.getPC();
        String cname = ii.getInvokedMethodClassName();
        String mname = ii.getInvokedMethodName();

        MethodInfo mi = RecoveryHelper.getStaticTarget(frame.getMethodInfo(), cname, mname);

        if (frame == ti.getTopFrame()) {
            // TODO inconsistent top
            if (frame.getTopPos() == 0 && mi.getArgumentsSize() != 0) {
                
            }
            frame.removeArguments(mi);
        } else {
            //System.out.println("" + frame + " " + frame.getDepth());
            frame.removeArguments(mi);
        }

        //System.out.println("" + frame + " " + frame.getDepth());
        RecoveryHelper.pushDefaultValue(frame, mi.getReturnType());

        ti.advancePC();

        return frame != null && frame.getDepth() == recoveryDepth;
    }

    public void toString(PrintWriter pw) {
        super.toString(pw);
        pw.printf(", at=%d, rt=%s", recoveryDepth, returnType);
    }
}
