package org.javelus.ares.recovery;

import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class StackBasedErrorTransformation extends ErrorTransformation {

    public StackBasedErrorTransformation(ThreadInfo ti,
            StackFrame handlerFrame, ClassInfo targetType) {
        super(ti, handlerFrame, targetType);
    }

}
