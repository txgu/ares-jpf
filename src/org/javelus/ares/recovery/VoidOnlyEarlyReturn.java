package org.javelus.ares.recovery;

import gov.nasa.jpf.vm.ThreadInfo;

public class VoidOnlyEarlyReturn extends EarlyReturn {

    public VoidOnlyEarlyReturn(ThreadInfo ti, int recoveryDepth) {
        super(ti, recoveryDepth);
    }

    public VoidOnlyEarlyReturn (ThreadInfo ti, int recoveryDepth, int expectedFinalDepth) {
        super(ti, recoveryDepth, expectedFinalDepth);
    }
}
