package org.javelus.ares.recovery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecoveryResultRanker {

    private RecoveryResultComparator acceptedComparator;

    private RecoveryResultComparator rejectedComparator;

    private List<RecoveryResult> acceptedResults;

    private List<RecoveryResult> rejectedResults;

    public RecoveryResultRanker(RecoveryResultComparator acceptedComparator,
            RecoveryResultComparator rejectedComparator) {
        this.acceptedComparator = acceptedComparator;
        this.rejectedComparator = rejectedComparator;
        acceptedResults = new ArrayList<RecoveryResult>();
        rejectedResults = new ArrayList<RecoveryResult>();
    }

    public RecoveryResult addAcceptedRecoveryResult(RecoveryResult result) {
        acceptedResults.add(result);
        Collections.sort(acceptedResults, acceptedComparator);
        return result;
    }

    public RecoveryResult addRejectedRecoveryResult(RecoveryResult result) {
        rejectedResults.add(result);
        Collections.sort(rejectedResults, rejectedComparator);
        return result;
    }

    public RecoveryResult getBestResult() {
        if (!acceptedResults.isEmpty()) {
            return acceptedResults.get(0);
        }

        if (!rejectedResults.isEmpty()) {
            return rejectedResults.get(0);
        }
        return null;
    }

    public void printResults() {
        System.out.println("Best result: " + this.getBestResult());
        System.out.println("Total accepted results: " + acceptedResults.size());
        for (int i=0; i<acceptedResults.size(); i++) {
            System.out.format("%4d: %s\n", i, acceptedResults.get(i));
        }
        System.out.println("Total rejected results: " + rejectedResults.size());
        for (int i=0; i<rejectedResults.size(); i++) {
            System.out.format("%4d: %s\n", i, rejectedResults.get(i));
        }
    }

    public List<RecoveryResult> getAcceptedResults() {
        return acceptedResults;
    }

    public List<RecoveryResult> getRejectedResults() {
        return rejectedResults;
    }
}
