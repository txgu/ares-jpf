package org.javelus.ares.recovery;

import gov.nasa.jpf.vm.ThreadInfo;

import java.io.PrintWriter;

public class NopAction extends AbstractRecoveryAction {
    private int exceptionRef;

    public NopAction(ThreadInfo ti, int exceptionRef) {
        super(ti, ti.getTopFrame().getDepth(), ti.getTopFrame().getDepth(), 0);
        this.exceptionRef = exceptionRef;
    }

    @Override
    public boolean recover() {
        ti.throwException(exceptionRef);
        return true;
    }

    public void toString(PrintWriter pw) {
        super.toString(pw);
        pw.printf(", e=%s", ti.getClassInfo(exceptionRef).getName());
    }
}
