package org.javelus.ares.recovery;

import gov.nasa.jpf.vm.ThreadInfo;

public interface RecoveryStrategy {

    void buildRecoveryActions(ThreadInfo ti, int causeExceptionRef);

    boolean hasNext();
    RecoveryAction next();

    void beginAction(ThreadInfo ti, RecoveryAction action);
    void endAction(ThreadInfo ti, RecoveryAction action);

    RecoveryResult caseCascadedError(ThreadInfo ti, RecoveryAction action);
    RecoveryResult caseInternalError(ThreadInfo ti, RecoveryAction action);
    RecoveryResult caseUninterpretableError(ThreadInfo ti, RecoveryAction action);
    RecoveryResult caseEarlyStop(ThreadInfo ti, RecoveryAction action);
    RecoveryResult caseTimeout(ThreadInfo ti, RecoveryAction action);
    RecoveryResult caseNoError(ThreadInfo ti, RecoveryAction action);

    RecoveryAction getBest();
    Object[] getEncodedBest();

    void printResults();
    void printActions();
}
