package org.javelus.ares.recovery;

import gov.nasa.jpf.Ares;

public class RecoveryActionEncoder {

    public static Object[] encode(RecoveryAction action) {
        if (action == null || action instanceof NopAction) {
            return null;
        }

        if (action instanceof ErrorTransformation) {
            Object[] result = new Object[2];

            result[0] = "ErrorTransformation";
            result[1] = Ares.getHeapAdapter().getJavaClass(((ErrorTransformation) action).getTargetType());
            return result;
        }

        if (action instanceof EarlyReturn) {
            Object[] result = new Object[2];

            result[0] = "EarlyReturn";
            result[1] = ((EarlyReturn) action).getRecoveryDepth() - 1; // skip DirectiCallFrame

            return result;
        }

        throw new RuntimeException("Should not reach here");
    }
}
