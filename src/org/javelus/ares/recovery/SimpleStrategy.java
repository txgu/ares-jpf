package org.javelus.ares.recovery;

import gov.nasa.jpf.Ares;
import gov.nasa.jpf.jvm.RecoveryHelper;
import gov.nasa.jpf.vm.AresHostStackFrame;
import gov.nasa.jpf.vm.AresVM;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ClassLoaderInfo;
import gov.nasa.jpf.vm.ExceptionHandler;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class SimpleStrategy implements RecoveryStrategy {

    private static final boolean CreateNop = false;

    private static final boolean debug = Ares.debugRecoveryStrategy;

    public static ClassInfo javaLangRuntimeException;

    public static synchronized ClassInfo javaLangRuntimeException () {
        if (javaLangRuntimeException == null) {
            javaLangRuntimeException = ClassLoaderInfo.getCurrentResolvedClassInfo("java.lang.RuntimeException");
        }
        return javaLangRuntimeException;
    }

    private RecoveryAction nextAction;

    private List<RecoveryAction> recoveryActions;

    private Iterator<RecoveryAction> recoveryActionsIterator;

    private RecoveryResultRanker ranker;

    private RecoveryExecutionGuard guard;

    public SimpleStrategy() {
        recoveryActions = new ArrayList<RecoveryAction>();
        ranker = new RecoveryResultRanker(new AcceptedRecoveryResultComparator(),
                new RejectedRecoveryResultComparator());
    }

    private void advance() {
        if (recoveryActionsIterator.hasNext()) {
            nextAction = recoveryActionsIterator.next();
            return;
        }
        nextAction = null;
    }

    @Override
    public void buildRecoveryActions(ThreadInfo ti, int causeExceptionRef) {
        StackFrame top = ti.getTopFrame();

        if (top == null) {
            throw new RuntimeException("Top frame is null");
        }

        Set<String> targetExceptions = new HashSet<String>();

        recoveryActions.clear();

        if (CreateNop) {
            recoveryActions.add(new NopAction(ti, causeExceptionRef));
        }

        ClassInfo causeCI = ti.getClassInfo(causeExceptionRef);

        ftet:for (StackFrame frame = top; frame.getDepth() > 0; frame = frame.getPrevious()) {
            MethodInfo callee = RecoveryHelper.getStaticCallee(ti, frame);

            if (callee == null) {
                continue;
            }

            MethodInfo mi = frame.getMethodInfo();

            Instruction insn = frame.getPC();
            ExceptionHandler[] exceptionHandlers = mi.getExceptions();
            if (exceptionHandlers != null) {
                int position = insn.getPosition();
                for (int i=0; i<exceptionHandlers.length; i++) {
                    ExceptionHandler handler = exceptionHandlers[i];
                    if ((position >= handler.getBegin()) && (position < handler.getEnd())) {
                        // checks if this type of exception is caught here (null means 'any')
                        String handledType = handler.getName();

                        if (handledType == null) {
                            continue;
                        }

                        ClassInfo ht = mi.getClassInfo().getClassLoaderInfo().loadClass(handledType);
                        if (!ht.isRegistered()) {
                            ht.registerClass(ti);
                        }

                        if (!ht.isInitialized()) {
                            ht.setInitialized();
                        }

                        if (debug) {
                            System.out.println("Add force-throwable target exception "+ ht);
                        }
                        recoveryActions.add(new ForceThrowableErrorTransformation(ti, frame, ht));
                        break ftet;
                    }
                }
            }
        }

        for (StackFrame frame = top; frame.getDepth() > 0; frame = frame.getPrevious()) {
            MethodInfo callee = RecoveryHelper.getStaticCallee(ti, frame);

            if (callee == null) {
                continue;
            }

            String[] exceptionNames = callee.getThrownExceptionClassNames();

            if (exceptionNames != null) {
                for (int i = 0; i < exceptionNames.length; i++) {
                    if (AresVM.isTrivial(exceptionNames[i])) {
                        continue;
                    }

                    ClassInfo ci = callee.getClassInfo().getClassLoaderInfo().loadClass(exceptionNames[i]);

                    if (!ci.isRegistered()) {
                        ci.registerClass(ti);
                    }

                    if (!ci.isInitialized()) {
                        ci.setInitialized();
                    }

                    if (causeCI == ci) {
                        continue;
                    }

                    if (!ci.isInstanceOf(javaLangRuntimeException)) {
                        StackFrame handlerFrame = RecoveryHelper.handlerFrameFor(ti, ci, frame); 
                        if (handlerFrame != null) {
                            if (targetExceptions.add(exceptionNames[i])) {
                                if (debug) {
                                    System.out.println("Add stack-based target exception "+ ci);
                                }
                                recoveryActions.add(new StackBasedErrorTransformation(ti, handlerFrame, ci));
                            }
                        }
                    }
                }
            }
        }

        int earlyReturnCount = 0;
        boolean hasVoidOnly = false;
        for (StackFrame frame = top; frame.getDepth() > 0; frame = frame.getPrevious()) {
            if (Ares.MaxEarlyReturnCount != -1 
                    && earlyReturnCount >= Ares.MaxEarlyReturnCount) {
                break;
            }

            MethodInfo mi = RecoveryHelper.getStaticCallee(ti, frame);
            if (mi != null) {
                if (mi.getReturnSize() == 0) {
                    hasVoidOnly = true;
                }
                recoveryActions.add(new EarlyReturn(ti, frame.getDepth()));
                earlyReturnCount++;
            }
        }

        // Build Void-Only Early Return
        if (!hasVoidOnly) {
            for (StackFrame frame = top; frame.getDepth() > 0; frame = frame.getPrevious()) {
                MethodInfo mi = RecoveryHelper.getStaticCallee(ti, frame);
                if (mi != null && mi.getReturnSize() == 0) {
                    recoveryActions.add(new VoidOnlyEarlyReturn(ti, frame.getDepth()));
                    break;
                }
            }
        }

        recoveryActionsIterator = recoveryActions.iterator();
        advance();
    }

    @Override
    public RecoveryResult caseEarlyStop(ThreadInfo ti, RecoveryAction action) {
        return ranker.addAcceptedRecoveryResult(new RecoveryResult(action, 
                ResultType.EARLY_STOP, 
                getActualFinalDepth(ti),
                guard.getSteps()));
    }

    @Override
    public RecoveryResult caseInternalError(ThreadInfo ti, RecoveryAction action) {
        RecoveryResult result = new RecoveryResult(action,
                ResultType.INTERNAL_ERROR,
                getActualFinalDepth(ti),
                guard.getSteps());

        if (isAccpeted(result)) {
            return ranker.addAcceptedRecoveryResult(result);
        }

        return ranker.addRejectedRecoveryResult(result);
    }

    @Override
    public RecoveryResult caseNoError(ThreadInfo ti, RecoveryAction action) {
        return ranker.addAcceptedRecoveryResult(new RecoveryResult(action,
                ResultType.NO_ERROR,
                getActualFinalDepth(ti),
                guard.getSteps()));
    }

    protected boolean isAccpeted(RecoveryResult result) {
        if (result.getResultType() == ResultType.UNINTERPRETABLE_ERROR) {
            return result.getExecutedInstructions() > Ares.AcceptThreshold; 
        }
        // For internal error and cascaded error
        return result.getExecutedInstructions() > Ares.AcceptThreshold &&
                result.getActualStoppingDepth() < result.getRecoveryDepth();
    }

    @Override
    public RecoveryResult caseCascadedError(ThreadInfo ti, RecoveryAction action) {
        RecoveryResult result = new RecoveryResult(action,
                ResultType.CASCADED_ERROR,
                getActualFinalDepth(ti),
                guard.getSteps());

        if (isAccpeted(result)) {
            return ranker.addAcceptedRecoveryResult(result);
        }

        return ranker.addRejectedRecoveryResult(result);

    }

    @Override
    public RecoveryResult caseTimeout(ThreadInfo ti, RecoveryAction action) {
        return ranker.addAcceptedRecoveryResult(new RecoveryResult(action,
                ResultType.TIMEOUT,
                getActualFinalDepth(ti),
                guard.getSteps()));
    }

    @Override
    public RecoveryResult caseUninterpretableError(ThreadInfo ti, RecoveryAction action) {
        RecoveryResult result = new RecoveryResult(action, 
                ResultType.UNINTERPRETABLE_ERROR, 
                getActualFinalDepth(ti),
                guard.getSteps());

        if (isAccpeted(result)) {
            return ranker.addAcceptedRecoveryResult(result);
        }

        return ranker.addRejectedRecoveryResult(result);
    }

    @Override
    public RecoveryAction getBest() {
        RecoveryResult bestResult = ranker.getBestResult();
        if (bestResult != null) {
            return bestResult.getAction();
        }
        return null;
    }

    public int getActualFinalDepth(ThreadInfo ti) {
        if (ti.getStackDepth() == 1) {
            return 0;
        }

        StackFrame frame = ti.getTopFrame();

        if (frame == null) {
            return 0;
        }

        for (; frame.getPrevious() != null; frame = frame.getPrevious()) {
            if (frame instanceof AresHostStackFrame) {
                return frame.getDepth();
            }
        }

        throw new RuntimeException("Should not reach here");
    }

    @Override
    public Object[] getEncodedBest() {
        return RecoveryActionEncoder.encode(getBest());
    }

    @Override
    public boolean hasNext() {
        return nextAction != null;
    }


    @Override
    public RecoveryAction next() {
        RecoveryAction ret = nextAction;
        advance();
        return ret;
    }

    @Override
    public void beginAction(ThreadInfo ti, RecoveryAction action) {
        guard = new RecoveryExecutionGuard(action);
        Ares.v().addListener(guard);
    }

    @Override
    public void endAction(ThreadInfo ti, RecoveryAction action) {
        Ares.v().removeListener(guard);
        guard = null;
    }

    @Override
    public void printResults() {
        ranker.printResults();
    }

    @Override
    public void printActions() {
        System.out.println("Total actions: " + recoveryActions.size());
        for (int i=0; i<recoveryActions.size(); i++) {
            System.out.format("%4d: %s\n", i, recoveryActions.get(i));
        }
    }


}
