package org.javelus.ares.recovery;

import gov.nasa.jpf.Ares;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.vm.AresHostStackFrame;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

import org.javelus.ares.EarlyStopException;
import org.javelus.ares.TimeoutException;
import org.javelus.ares.UninterpretableException;
/**
 * Count executed instructions
 * @author t
 *
 */
public class RecoveryExecutionGuard extends ListenerAdapter {

    private static final boolean CHECK_STACK = false;

    private RecoveryAction action;

    private int step = 0;

    public RecoveryExecutionGuard(RecoveryAction action){
        this.action = action;
    }

    public int getSteps() {
        return step;
    }
    
    @Override
    public void executeInstruction(VM vm, ThreadInfo currentThread,
            Instruction instructionToExecute) {
        step++;

        if (step >= Ares.MaxStep) {
            if (currentThread.getPendingException() == null) {
                throw new TimeoutException(String.format("We have reach the max step %d.", step));
            }
        }
        
        StackFrame frame = currentThread.getTopFrame();
        if (frame instanceof AresHostStackFrame) {
            AresHostStackFrame ahsf = (AresHostStackFrame) frame;
            if (!ahsf.isExecutable()) {
                throw new UninterpretableException("A host stack frame of " + frame.getMethodInfo() + " is not executable.");
            }
        }

        if (CHECK_STACK) {
            int stackDepth = currentThread.getStackDepth();

            // XXX getStackDepth is the number of frames
            // ExpectedFinalDepth is the offset of the frame
            // So, we need add one.
            if (stackDepth == action.getExpectedStoppingDepth() + 1) { 
                if (currentThread.getPendingException() == null) {
                    throw new EarlyStopException(String.format("We have reach the expected final depth %d.", 
                            action.getExpectedStoppingDepth()));
                }
            }
        }
    }
}
