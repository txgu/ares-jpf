package org.javelus.ares.recovery;

public enum ResultType {
    NO_ERROR,
    EARLY_STOP, /* this is not used now*/
    TIMEOUT,
    UNINTERPRETABLE_ERROR,
    CASCADED_ERROR,
    INTERNAL_ERROR,
}
