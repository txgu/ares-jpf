package org.javelus.ares.recovery;

import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

import java.io.PrintWriter;

public class ErrorTransformation extends AbstractRecoveryAction {
    private ClassInfo targetType;

    public ErrorTransformation(ThreadInfo ti, StackFrame handlerFrame, ClassInfo targetType) {
        super(ti, ti.getTopFrame().getDepth(), handlerFrame.getDepth(), handlerFrame.getDepth() - 1);
        this.targetType = targetType;
    }

    @Override
    public boolean recover() {
        if (!targetType.isRegistered()) {
            targetType.registerClass(ti);
        }

        if (!targetType.isInitialized()) {
            targetType.setInitialized();
        }

        Instruction pc = ti.createAndThrowException(targetType, "error transformation into " + targetType.getName());
        return pc != null;
    }

    public ClassInfo getTargetType() {
        return targetType;
    }

    public void toString(PrintWriter pw) {
        super.toString(pw);
        pw.printf(", tt=%s",  targetType.getName());
    }
}
