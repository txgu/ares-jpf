package org.javelus.ares.recovery;

import java.util.Comparator;


public class SimpleRecoveryResultComparator implements RecoveryResultComparator {

    public static int leftIsBetter = -1;
    public static int rightIsBetter = 1;

    /**
     * <p>
     * Currently there are only two kinds of recovery actions: error transformation and early return
     * <ol>
     * <li>rejected: error transformation is better than early return.
     * <li>accepted: early return is better than error transformation.
     * </ol>
     * </p>
     * <p>
     * There only 3 cases:
     * <ol>
     *  <li>a1 = ET, a2 = ET
     *  <li>a1 = ER, a2 = ET
     *  <li>a1 = ET, a2 = ER
     * </ol>
     * </p>
     */
    public static Comparator<RecoveryAction> acceptedActionComparator = new Comparator<RecoveryAction>() {
        @Override
        public int compare(RecoveryAction a1, RecoveryAction a2) {
            if (a1 instanceof ErrorTransformation) {
                if (a2 instanceof ErrorTransformation) {
                    return ((ErrorTransformation) a1).getTargetType().getName().compareTo(((ErrorTransformation) a2).getTargetType().getName());
                } else {
                    return leftIsBetter;
                }
            } else {
                if (a2 instanceof ErrorTransformation) {
                    return rightIsBetter;
                }
            }
            throw new RuntimeException(String.format("Unknown type of recovery actions: %s and %s", a1.getClass(), a2.getClass()));
        }
    };

    public static Comparator<RecoveryAction> rejectedActionComparator = new Comparator<RecoveryAction>() {
        @Override
        public int compare(RecoveryAction a1, RecoveryAction a2) {
            if (a1 instanceof ErrorTransformation) {
                if (a2 instanceof ErrorTransformation) {
                    return ((ErrorTransformation) a1).getTargetType().getName().compareTo(((ErrorTransformation) a2).getTargetType().getName());
                } else {
                    return rightIsBetter;
                }
            } else {
                if (a2 instanceof ErrorTransformation) {
                    return leftIsBetter;
                }
            }
            throw new RuntimeException(String.format("Unknown type of recovery actions: %s and %s", a1.getClass(), a2.getClass()));
        }
    };

    /**
     * Better is smaller, so return -1
     * <p><ol>
     *  <li>Compare the result type: ideal > timeout > unsupported behavior > cascaded exception</li> 
     *  <li>Compare the stacks have been dropped, less is better</li> 
     *  <li>Compare the left recover context, less is better.</li> 
     *  <li>Compare the type of error handler
     *   <ol>
     *     <li>Ideal: early return is better than error transformation </li>
     *     <li>Exceptional: error transformation is better than early return</li>
     *   </ol>
     *  </li>
     * </ol></p>
     * 
     * <p>
     * Why: The safer, the more aggressive recover action
     * <ul> 
     *  <li>Ideal stopping is safer than exceptional stopping</li>
     *  <li>Early Return is more aggressive than Error Transformation</li>
     * </ul>
     * </p>
     */
    @Override
    public int compare(RecoveryResult o1, RecoveryResult o2) {

        ResultType rt1 = o1.getResultType();
        ResultType rt2 = o2.getResultType();

        // 1). compare the result type
        if (rt1 != rt2) {
            return rt1.ordinal() - rt2.ordinal();
        }

        int d1 = o1.getDroppedLength();
        int d2 = o2.getDroppedLength();

        if (d1 != d2) {
            return d1 - d2;
        }

        int c1 = o1.getActualStoppingDepth();
        int c2 = o2.getActualStoppingDepth();

        if (c1 != c2) {
            return c1 - c2;
        }

        RecoveryAction a1 = o1.getAction();
        RecoveryAction a2 = o2.getAction();

        if (rt1 == ResultType.NO_ERROR) {
            return rejectedActionComparator.compare(a1, a2);
        } else {
            return acceptedActionComparator.compare(a1, a2);
        }
    }

}
