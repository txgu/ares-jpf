package org.javelus.ares.recovery;

public class RejectedRecoveryResultComparator implements RecoveryResultComparator {

    /**
     * return -1 if o1 is better than o2
     */
    @Override
    public int compare(RecoveryResult o1, RecoveryResult o2) {
        int s1 = o1.getExecutedInstructions();
        int s2 = o2.getExecutedInstructions();

        if (s1 > s2) {
            return -1;
        }

        if (s1 < s2) {
            return 1;
        }

        int d1 = o1.getDroppedLength();
        int d2 = o2.getDroppedLength();

        if (d1 < d2) {
            return -1;
        }

        if (d1 > d2) {
            return 1;
        }

        return SimpleRecoveryResultComparator.rejectedActionComparator.compare(o1.getAction(), o2.getAction());
    }

}
