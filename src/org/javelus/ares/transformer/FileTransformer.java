package org.javelus.ares.transformer;

import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.ThreadInfo;

import java.io.File;

import org.javelus.ares.HeapAdapter;
import org.javelus.ares.ObjectTransformer;

public class FileTransformer implements ObjectTransformer<File> {

    static final String FILE_NAME = "filename";

    public void transform(ThreadInfo ti, HeapAdapter ha, ElementInfo ei, File file) {
        FieldInfo fi = ei.getClassInfo().getDeclaredInstanceField("filename");
        System.out.println("Transforming a file object " + file);
        ha.setObjectObjectField(ti, ei, fi, ha.getJPFObject(ti, file.getPath()));
    }
}
