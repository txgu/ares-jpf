package org.javelus.ares.jvm.bytecode;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class DIRECTCALLRETURN extends gov.nasa.jpf.jvm.bytecode.DIRECTCALLRETURN {

    public Instruction execute(ThreadInfo ti) {
        if (ti.getStackDepth() == 1) { // thread exit point
            // this can execute several times because of the different locks
            // involved
            ti.popDirectCallFrame();
            return null;
        } else {
            // pop the current frame but do not advance the new top frame, and
            // do
            // not touch its operand stack

            StackFrame frame = ti.popDirectCallFrame();
            return frame.getPC();
        }
    }
}
