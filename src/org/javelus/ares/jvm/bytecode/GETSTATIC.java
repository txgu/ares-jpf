package org.javelus.ares.jvm.bytecode;

import gov.nasa.jpf.Ares;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.LoadOnJPFRequired;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class GETSTATIC extends gov.nasa.jpf.jvm.bytecode.GETSTATIC {

    public GETSTATIC(String fieldName, String classType, String fieldDescriptor) {
        super(fieldName, classType, fieldDescriptor);
    }

    @Override
    public Instruction execute(ThreadInfo ti) {
        FieldInfo fieldInfo;

        //--- check if this causes a class load by a user defined classloader
        try {
            fieldInfo = getFieldInfo();
        } catch (LoadOnJPFRequired lre) {
            return ti.getPC();
        }

        if (fieldInfo == null) {
            return ti.createAndThrowException("java.lang.NoSuchFieldError",
                    (className + '.' + fname));
        }

        //--- check if this has to trigger class initialization
        ClassInfo ciField = fieldInfo.getClassInfo();
        if (!mi.isClinit(ciField) && ciField.pushRequiredClinits(ti)) {
            // note - this returns the next insn in the topmost clinit that just got pushed
            return ti.getPC();
        }
        ElementInfo eiFieldOwner = ciField.getStaticElementInfo();

        Object fieldAttr = eiFieldOwner.getFieldAttr(fieldInfo);
        StackFrame frame = ti.getModifiableTopFrame();

        if (fieldAttr == null && eiFieldOwner.hasObjectAttr()) {
            Ares.getHeapAdapter().loadStaticField(ti, ciField, fieldInfo);
            // we modified it in loadStaticField, get the elementinfo again.
            eiFieldOwner = ciField.getStaticElementInfo();
        }

        if (size == 1) {
            int ival = eiFieldOwner.get1SlotField(fieldInfo);
            lastValue = ival;

            if (fieldInfo.isReference()) {
                frame.pushRef(ival);
            } else {
                frame.push(ival);
            }

            if (fieldAttr != null) {
                frame.setOperandAttr(fieldAttr);
            }

        } else {
            long lval = eiFieldOwner.get2SlotField(fieldInfo);
            lastValue = lval;

            frame.pushLong(lval);

            if (fieldAttr != null) {
                frame.setLongOperandAttr(fieldAttr);
            }
        }

        return getNext(ti);
    }



}
