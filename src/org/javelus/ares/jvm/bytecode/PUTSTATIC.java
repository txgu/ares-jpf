package org.javelus.ares.jvm.bytecode;

import gov.nasa.jpf.util.InstructionState;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.LoadOnJPFRequired;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class PUTSTATIC extends gov.nasa.jpf.jvm.bytecode.PUTSTATIC {

    public PUTSTATIC(String fieldName, String clsDescriptor,
            String fieldDescriptor) {
        super(fieldName, clsDescriptor, fieldDescriptor);
    }

    @Override
    public Instruction execute (ThreadInfo ti) {
        StackFrame frame = ti.getModifiableTopFrame();
        FieldInfo fieldInfo;

        //--- check if this causes a class load by a user defined classloader
        try {
            fieldInfo = getFieldInfo();
        } catch (LoadOnJPFRequired lre) {
            return ti.getPC();
        }

        if (fieldInfo == null) {
            return ti.createAndThrowException("java.lang.NoSuchFieldError", (className + '.' + fname));
        }

        //--- check if this has to trigger class initialization
        ClassInfo ciField = fieldInfo.getClassInfo();
        if (!mi.isClinit(ciField) && ciField.pushRequiredClinits(ti)) {
            return ti.getPC(); // this returns the next insn in the topmost clinit that just got pushed
        }
        ElementInfo eiFieldOwner = ciField.getModifiableStaticElementInfo();

        // check if this gets re-executed from a exposure CG (which already did the assignment
        if (frame.getAndResetFrameAttr(InstructionState.class) == null){
            lastValue = PutHelper.setField(ti, frame, eiFieldOwner, fieldInfo);
        }

        popOperands(frame);
        return getNext();
    }
}
