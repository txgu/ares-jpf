package org.javelus.ares.jvm.bytecode;

import gov.nasa.jpf.jvm.bytecode.JVMInstructionVisitor;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.LoadOnJPFRequired;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Types;

public class INSTANCEOF extends Instruction {
    private String type;


    /**
     * typeName is of a/b/C notation
     */
    public INSTANCEOF (String typeName){
        type = Types.getTypeSignature(typeName, false);
    }

    public Instruction execute (ThreadInfo ti) {
        if(Types.isReferenceSignature(type)) {
            String t;
            if(Types.isArray(type)) {
                // retrieve the component terminal
                t = Types.getComponentTerminal(type);
            } else {
                t = type;
            }

            // resolve the referenced class
            try {
                ti.resolveReferencedClass(t);
            } catch(LoadOnJPFRequired lre) {
                return ti.getPC();
            }
        }

        StackFrame frame = ti.getModifiableTopFrame();
        int objref = frame.pop();

        if (objref == MJIEnv.NULL) {
            frame.push(0);
        } else if (ti.getClassInfo(objref).isInstanceOf(type)) {
            frame.push(1);
        } else {
            frame.push(0);
        }

        return getNext(ti);
    }

    public String getType() {
        return type;
    }

    public int getLength() {
        return 3; // opcode, index1, index2
    }

    public int getByteCode () {
        return 0xC1;
    }

    public void accept(JVMInstructionVisitor insVisitor) {
        insVisitor.visit(this);
    }
}
