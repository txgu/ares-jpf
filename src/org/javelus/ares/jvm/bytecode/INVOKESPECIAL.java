//
package org.javelus.ares.jvm.bytecode;

import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.LoadOnJPFRequired;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;


/**
 * Invoke instance method; special handling for superclass, private,
 * and instance initialization method invocations
 * ..., objectref, [arg1, [arg2 ...]] => ...
 *
 * invokedMethod is supposed to be constant (ClassInfo can't change)
 */
public class INVOKESPECIAL extends gov.nasa.jpf.jvm.bytecode.INVOKESPECIAL {

    public INVOKESPECIAL (String clsDescriptor, String methodName, String signature){
        super(clsDescriptor, methodName, signature);
    }


    public Instruction execute (ThreadInfo ti) {
        int argSize = getArgSize();
        int objRef = ti.getCalleeThis( argSize);
        lastObj = objRef;

        // we don't have to check for NULL objects since this is either a ctor, a 
        // private method, or a super method
        // WE NEED CHECK as a private method may called in a public method with different receiver.
        if (objRef == MJIEnv.NULL) {
            lastObj = MJIEnv.NULL;
            return ti.createAndThrowException("java.lang.NullPointerException", "Calling '" + mname + "' on null object");
        }

        MethodInfo callee;

        try {
            callee = getInvokedMethod(ti);
        } catch(LoadOnJPFRequired rre) {
            return ti.getPC();
        }      

        if (callee == null){
            return ti.createAndThrowException("java.lang.NoSuchMethodException", "Calling " + cname + '.' + mname);
        }

        ElementInfo ei = ti.getElementInfo(objRef);
        if (callee.isSynchronized()){
            ei = ti.getScheduler().updateObjectSharedness(ti, ei, null); // locks most likely belong to shared objects
            if (reschedulesLockAcquisition(ti, ei)){
                return this;
            }
        }

        setupCallee( ti, callee); // this creates, initializes and pushes the callee StackFrame

        return ti.getPC(); // we can't just return the first callee insn if a listener throws an exception
    }

}
