package org.javelus.ares.jvm.bytecode;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class MONITOREXIT extends gov.nasa.jpf.jvm.bytecode.MONITOREXIT {

    public Instruction execute (ThreadInfo ti) {
        // bottom half or monitorexit proceeded
        StackFrame frame = ti.getModifiableTopFrame();
        frame.pop();

        return getNext(ti);
    }
}
