package org.javelus.ares.jvm.bytecode;

import gov.nasa.jpf.util.InstructionState;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class PUTFIELD extends gov.nasa.jpf.jvm.bytecode.PUTFIELD {

    public PUTFIELD(String fieldName, String clsDescriptor,
            String fieldDescriptor) {
        super(fieldName, clsDescriptor, fieldDescriptor);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Instruction execute (ThreadInfo ti) {
        StackFrame frame = ti.getModifiableTopFrame();
        int objRef = frame.peek( size);
        lastThis = objRef;

        if (objRef == MJIEnv.NULL) {
            return ti.createAndThrowException("java.lang.NullPointerException", "referencing field '" + fname + "' on null object");
        }

        ElementInfo eiFieldOwner = ti.getModifiableElementInfo(objRef);
        FieldInfo fieldInfo = getFieldInfo();
        if (fieldInfo == null) {
            return ti.createAndThrowException("java.lang.NoSuchFieldError", "no field " + fname + " in " + eiFieldOwner);
        }

        // this might be re-executed
        if (frame.getAndResetFrameAttr(InstructionState.class) == null){
            lastValue = PutHelper.setField(ti, frame, eiFieldOwner, fieldInfo);
        }

        popOperands(frame);
        return getNext();
    }

}
