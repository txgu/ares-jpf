//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
// 
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
// 
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//
package org.javelus.ares.jvm.bytecode;

import gov.nasa.jpf.util.InstructionState;
import gov.nasa.jpf.vm.ArrayIndexOutOfBoundsExecutiveException;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

import org.javelus.ares.HeapAdapter;


/**
 * Store into reference array
 * ..., arrayref, index, value  => ...
 */
public class AASTORE extends gov.nasa.jpf.jvm.bytecode.AASTORE {

    @Override
    public Instruction execute(ThreadInfo ti) {
        StackFrame frame = ti.getModifiableTopFrame();
        int refValue = frame.peek();
        int idx = frame.peek(1);
        int aref = frame.peek(2);

        index = idx;

        if (aref == MJIEnv.NULL) {
            return ti.createAndThrowException("java.lang.NullPointerException");
        }

        ElementInfo eiArray = ti.getModifiableElementInfo(aref);

        if (!ti.isFirstStepInsn()) { // we only need to check this once
            Instruction xInsn = checkArrayStoreException(ti, frame, eiArray);
            if (xInsn != null) {
                return xInsn;
            }
        }

        // check if this gets re-executed from an exposure CG
        if (frame.getAndResetFrameAttr(InstructionState.class) == null) {
            try {
                //Object attr = frame.getOperandAttr();
                eiArray.checkArrayBounds(idx);
                eiArray.setReferenceElement(idx, refValue);
                eiArray.setElementAttrNoClone(idx, HeapAdapter.LOADED_FLAG);

            } catch (ArrayIndexOutOfBoundsExecutiveException ex) {
                return ex.getInstruction();
            }

        }

        popValue(frame);
        frame.pop(2);

        return getNext(ti);
    }
}
