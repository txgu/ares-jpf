package org.javelus.ares.jvm.bytecode;

import org.javelus.ares.UninterpretableException;

import gov.nasa.jpf.Ares;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.NativeMethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;

public class EXECUTENATIVE extends gov.nasa.jpf.jvm.bytecode.EXECUTENATIVE {

    public EXECUTENATIVE (){}

    public EXECUTENATIVE (NativeMethodInfo mi){
        super(mi);
    }

    public Instruction execute (ThreadInfo ti) {
        // we don't have to enter/leave or push/pop a frame, that's all done
        // in NativeMethodInfo.execute()
        // !! don't re-enter if this is reexecuted !!
        try {
            return executedMethod.executeNative(ti);
        } catch (RuntimeException e) {
            if (Ares.PrintStackTrace) {
                e.printStackTrace();
            }
            throw new UninterpretableException("Currently, we cannot successfully execute native method " + executedMethod.getUniqueName());
        }
    }
}
