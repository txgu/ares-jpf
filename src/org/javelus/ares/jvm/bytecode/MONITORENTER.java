package org.javelus.ares.jvm.bytecode;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class MONITORENTER extends gov.nasa.jpf.jvm.bytecode.MONITORENTER {

    public Instruction execute (ThreadInfo ti) {
        //--- bottom half or lock acquisition succeeded without transition break
        StackFrame frame = ti.getModifiableTopFrame(); // now we need to modify it
        frame.pop();

        return getNext(ti);
    }  
}
