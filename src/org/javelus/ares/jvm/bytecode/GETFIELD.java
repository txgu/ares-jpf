package org.javelus.ares.jvm.bytecode;

import gov.nasa.jpf.Ares;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class GETFIELD extends gov.nasa.jpf.jvm.bytecode.GETFIELD {

    public GETFIELD(String fieldName, String classType, String fieldDescriptor) {
        super(fieldName, classType, fieldDescriptor);
    }

    @Override
    public Instruction execute(ThreadInfo ti) {
        StackFrame frame = ti.getModifiableTopFrame();
        int objRef = frame.peek(); // don't pop yet, we might re-enter
        lastThis = objRef;

        //--- check for obvious exceptions
        if (objRef == MJIEnv.NULL) {
            return ti.createAndThrowException("java.lang.NullPointerException",
                    "referencing field '" + fname + "' on null object");
        }

        ElementInfo eiFieldOwner = ti.getElementInfo(objRef);
        FieldInfo fieldInfo = getFieldInfo();
        if (fieldInfo == null) {
            return ti.createAndThrowException("java.lang.NoSuchFieldError",
                    "referencing field '" + fname + "' in " + eiFieldOwner);
        }

        frame.pop(); // Ok, now we can remove the object ref from the stack
        Object fieldAttr = eiFieldOwner.getFieldAttr(fieldInfo);

        if (fieldAttr == null && eiFieldOwner.getObjectAttr() != null) {
            Ares.getHeapAdapter().loadInstanceField(ti, objRef, fieldInfo);
            eiFieldOwner = ti.getElementInfo(objRef);
        }

        // We could encapsulate the push in ElementInfo, but not the GET, so we keep it at the same level
        if (fieldInfo.getStorageSize() == 1) { // 1 slotter
            int ival = eiFieldOwner.get1SlotField(fieldInfo);
            lastValue = ival;

            if (fieldInfo.isReference()) {
                frame.pushRef(ival);
            } else {
                frame.push(ival);
            }

            if (fieldAttr != null) {
                frame.setOperandAttr(fieldAttr);
            }

        } else {  // 2 slotter
            long lval = eiFieldOwner.get2SlotField(fieldInfo);
            lastValue = lval;

            frame.pushLong( lval);
            if (fieldAttr != null) {
                frame.setLongOperandAttr(fieldAttr);
            }
        }

        return getNext(ti);
    }



}
