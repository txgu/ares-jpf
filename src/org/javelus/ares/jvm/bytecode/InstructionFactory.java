package org.javelus.ares.jvm.bytecode;

import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.NativeMethodInfo;

public class InstructionFactory extends gov.nasa.jpf.jvm.bytecode.InstructionFactory {

    @Override
    public Instruction aaload() {
        return new AALOAD();
    }

    @Override
    public Instruction aastore() {
        return new AASTORE();
    }

    @Override
    public Instruction daload() {
        return new DALOAD();
    }

    @Override
    public Instruction dastore() {
        return new DASTORE();
    }

    @Override
    public Instruction directcallreturn() {
        return new DIRECTCALLRETURN();
    }

    @Override
    public Instruction executenative(NativeMethodInfo mi){
        return new EXECUTENATIVE(mi);
    }

    @Override
    public Instruction faload() {
        return new FALOAD();
    }

    @Override
    public Instruction fastore() {
        return new FASTORE();
    }

    @Override
    public Instruction getfield(String fieldName, String clsName, String fieldDescriptor){
        return new GETFIELD(fieldName, clsName, fieldDescriptor);
    }

    @Override
    public Instruction getstatic(String fieldName, String clsName,
            String fieldDescriptor) {
        return new GETSTATIC(fieldName, clsName, fieldDescriptor);
    }

    @Override
    public Instruction iaload() {
        return new IALOAD();
    }

    @Override
    public Instruction instanceof_(String clsName) {
        return new INSTANCEOF(clsName);
    }

    @Override
    public Instruction invokespecial(String clsName, String methodName,
            String methodSignature) {
        return new INVOKESPECIAL(clsName, methodName, methodSignature);
    }

    @Override
    public Instruction monitorenter() {
        return new MONITORENTER();
    }

    @Override
    public Instruction monitorexit() {
        return new MONITOREXIT();
    }

    @Override
    public Instruction iastore() {
        return new IASTORE();
    }

    @Override
    public Instruction invokeclinit(ClassInfo ci) {
        return new INVOKECLINIT(ci);
    }

    @Override
    public Instruction laload() {
        return new LALOAD();
    }

    @Override
    public Instruction lastore() {
        return new LASTORE();
    }

    @Override
    public Instruction putfield(String fieldName, String clsName, String fieldDescriptor){
        return new PUTFIELD(fieldName, clsName, fieldDescriptor);
    }

    @Override
    public Instruction putstatic(String fieldName, String clsName,
            String fieldDescriptor) {
        return new PUTSTATIC(fieldName, clsName, fieldDescriptor);
    }

    @Override
    public Instruction saload() {
        return new SALOAD();
    }

    @Override
    public Instruction sastore() {
        return new SASTORE();
    }
}
