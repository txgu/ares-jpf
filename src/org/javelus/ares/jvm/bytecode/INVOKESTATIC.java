//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
// 
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
// 
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//
package org.javelus.ares.jvm.bytecode;


import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.LoadOnJPFRequired;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;


/**
 * Invoke a class (static) method
 * ..., [arg1, [arg2 ...]]  => ...
 */
public class INVOKESTATIC extends gov.nasa.jpf.jvm.bytecode.INVOKESTATIC {

    protected INVOKESTATIC (String clsDescriptor, String methodName, String signature){
        super(clsDescriptor, methodName, signature);
    }

    public Instruction execute (ThreadInfo ti) {
        MethodInfo callee;

        try {
            callee = getInvokedMethod(ti);
        } catch (LoadOnJPFRequired lre) {
            return ti.getPC();
        }

        if (callee == null) {
            return ti.createAndThrowException("java.lang.NoSuchMethodException", cname + '.' + mname);
        }

        // this can be actually different than (can be a base)
        ClassInfo ciCallee = callee.getClassInfo();

        if (ciCallee.pushRequiredClinits(ti)) {
            // do class initialization before continuing
            // note - this returns the next insn in the topmost clinit that just got pushed
            return ti.getPC();
        }

        setupCallee( ti, callee); // this creates, initializes and pushes the callee StackFrame

        return ti.getPC(); // we can't just return the first callee insn if a listener throws an exception
    }

}

