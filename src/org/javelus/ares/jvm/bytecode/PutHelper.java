package org.javelus.ares.jvm.bytecode;

import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

import org.javelus.ares.HeapAdapter;

public class PutHelper {

    protected static boolean hasNewValue (ThreadInfo ti, StackFrame frame, ElementInfo eiFieldOwner, FieldInfo fi){
        Object valAttr = null;
        int fieldSize = fi.getStorageSize();

        if (fieldSize == 1){
            valAttr = frame.getOperandAttr();
            int val = frame.peek();
            if (eiFieldOwner.get1SlotField(fi) != val){
                return true;
            }

        } else {
            valAttr = frame.getLongOperandAttr();
            long val = frame.peekLong();
            if (eiFieldOwner.get2SlotField(fi) != val){
                return true;
            }
        }

        if (eiFieldOwner.getFieldAttr(fi) != valAttr){
            return true;
        }

        return false;
    }

    protected static int setReferenceField (ThreadInfo ti, StackFrame frame, ElementInfo eiFieldOwner, FieldInfo fi){
        Object valAttr = frame.getOperandAttr();
        int val = frame.peek();
        eiFieldOwner.set1SlotField(fi, val);
        eiFieldOwner.setFieldAttr(fi, valAttr);
        return val;
    }

    protected static long setField (ThreadInfo ti, StackFrame frame, ElementInfo eiFieldOwner, FieldInfo fi){
        int fieldSize = fi.getStorageSize();

        if (eiFieldOwner.hasObjectAttr()) {
            eiFieldOwner.addFieldAttr(fi, HeapAdapter.LOADED_FLAG);
        }

        if (fieldSize == 1){
            Object valAttr = frame.getOperandAttr();
            int val = frame.peek();
            eiFieldOwner.set1SlotField(fi, val);
            eiFieldOwner.addFieldAttr(fi, valAttr);
            return val;
        } else {
            Object valAttr = frame.getLongOperandAttr();
            long val = frame.peekLong();
            eiFieldOwner.set2SlotField(fi, val);
            eiFieldOwner.addFieldAttr(fi, valAttr);
            return val;
        }
    }
}
