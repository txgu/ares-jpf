package org.javelus.ares;

import gov.nasa.jpf.Ares;
import gov.nasa.jpf.vm.DirectCallStackFrame;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.AresStackFrameAdapter64;
import gov.nasa.jpf.vm.ThreadInfo;

public class StackBuilder {


    private static final boolean debug = Ares.debugStackBuilder;

    private HeapAdapter ha;

    public StackBuilder(HeapAdapter ha) {
        this.ha = ha;
    }

    /**
     * TODO remove redundant try-catch
     * @param ti
     * @param exception
     * @param descs
     */
    public void pushStackFrames(ThreadInfo ti, Object exception, StackFrameDesc[] descs) {
        // build entry
        if (descs.length > 0) {
            pushDirectStackFrame(ti, descs[descs.length-1]);
            // build others
            for(int i=descs.length-1; i>=0; i--) {
                try {
                    pushStackFrame(ti, descs[i]);
                } catch (AresException e) {
                    System.out.println("**************************");
                    System.out.println("Get AresException during building stack");
                    clearPushedStackFrames(ti);
                    System.out.println("**************************");
                }
            }
        }

        if (debug) {
            System.out.println("Begin printing stacks just after finishing pushing frames.");
            for (StackFrame frame = ti.getTopFrame(); frame != null; frame = frame.getPrevious()) {
                System.out.format("%4d: %s %s\n", frame.getDepth(), frame, frame.getClassInfo());
            }
        }
    }

    private void clearPushedStackFrames(ThreadInfo ti) {
        StackFrame top = ti.getTopFrame();
        while (!(top instanceof DirectCallStackFrame)) {
            top = ti.popAndGetTopFrame();
        }
    }

    public void pushDirectStackFrame(ThreadInfo ti,
            StackFrameDesc javaFrame) {
        if (ti.getTopFrame() != null) {
            throw new RuntimeException("Sanity check failed! Top frame is " + ti.getTopFrame().toString() + ", mi=" + ti.getTopFrame().getMethodInfo());
        }

        MethodInfo mi = ha.getJPFMethod(ti, javaFrame.method);
        DirectCallStackFrame frame = mi.createDirectCallStackFrame(ti, 0);
        ti.pushFrame(frame);
        // Setup pc
        Instruction pc = frame.getMethodInfo().getInstruction(0);
        frame.setPC(pc); // first invoke

        // Push phantom call arguments

        int argOffset = 0;
        if (!mi.isStatic()) {
            argOffset = frame.setReferenceArgument(argOffset, MJIEnv.NULL, null);
        }

        String[] typeNames = mi.getArgumentTypeNames();
        for (int i=0; i<typeNames.length; i++){
            String type = typeNames[i];
            if(type.equals("long")){
                argOffset = frame.setLongArgument(argOffset, 0L, null);
            } else if (type.equals("double")) {
                argOffset = frame.setDoubleArgument(argOffset, 0.0D, null);
            } else if (type.equals("float")) {
                argOffset = frame.setFloatArgument(argOffset, 0.0F, null);
            } else if (type.equals("int")
                    || type.equals("char")
                    || type.equals("byte")
                    || type.equals("short")) {
                argOffset = frame.setArgument(argOffset, 0, null);
            } else {
                argOffset = frame.setReferenceArgument(argOffset, MJIEnv.NULL, null);
            }
        }



        // Setup monitor
        ti.enter();
    }

    public void printMethodCode(MethodInfo mi) {
        if (mi.getInstructions() != null) {
            for (Instruction insn : mi.getInstructions()) {
                System.out.format("%08d:%s\n",insn.getPosition(), insn);
            }
        } else {
            System.out.println("Method " + mi + " has no code!");
        }
    }

    public void pushStackFrame(ThreadInfo ti, StackFrameDesc javaFrame) {
        MethodInfo mi = ha.getJPFMethod(ti, javaFrame.method);

        AresStackFrameAdapter64 frame = new AresStackFrameAdapter64(mi);
        frame.setExecutable(true);
        ti.pushFrame(frame);

        if (mi.isMJI()){
            //throw new RuntimeException("Cannot setup native method.");
            frame.setExecutable(false);
        } else {
            Instruction pc = mi.getInstructionAt(javaFrame.pc);
            if (pc == null) {
                //printMethodCode(mi);
                //throw new RuntimeException(String.format("Instruction is null at %d in method %s", javaFrame.pc, mi));
                frame.setExecutable(false);
            }
            frame.setPC(pc);
        }

        // Setup locals
        try {
            frame.setJavaFrame(ti, ha, javaFrame);
            // TODO synchronized method will use getThis, 
            // We must call this after stack has been sketched
            // Setup monitor
            ti.enter();
        } catch (AresException e) {
            frame.setExecutable(false);
        }
    }
}

