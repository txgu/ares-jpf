package org.javelus.ares;

public class UninterpretableException extends AresException {

    /**
     * 
     */
    private static final long serialVersionUID = 7540849773188047494L;

    public UninterpretableException() {
        super();
    }

    public UninterpretableException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public UninterpretableException(String message, Throwable cause) {
        super(message, cause);
    }

    public UninterpretableException(String message) {
        super(message);
    }

    public UninterpretableException(Throwable cause) {
        super(cause);
    }

}
