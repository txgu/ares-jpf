package org.javelus.ares;

public class AresException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -9082472921258814167L;

    public AresException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public AresException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

    public AresException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public AresException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public AresException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }


}
