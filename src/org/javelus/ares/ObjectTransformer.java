package org.javelus.ares;

import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.ThreadInfo;

public interface ObjectTransformer<Java> {
    void transform(ThreadInfo ti, HeapAdapter ha, ElementInfo jpf, Java java);
}
