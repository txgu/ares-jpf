package org.javelus.ares;

public class EarlyStopException extends AresException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public EarlyStopException() {
        super();
    }

    public EarlyStopException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public EarlyStopException(String message, Throwable cause) {
        super(message, cause);
    }

    public EarlyStopException(String message) {
        super(message);
    }

    public EarlyStopException(Throwable cause) {
        super(cause);
    }

}
