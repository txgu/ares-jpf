package org.javelus.ares;

import gov.nasa.jpf.Ares;
import gov.nasa.jpf.jvm.DelegateClassLoaderInfo;
import gov.nasa.jpf.jvm.AresClassLoaderFileContainer;
import gov.nasa.jpf.jvm.AresClassLoaderInfo;
import gov.nasa.jpf.vm.BooleanArrayFields;
import gov.nasa.jpf.vm.ByteArrayFields;
import gov.nasa.jpf.vm.CharArrayFields;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ClassLoaderInfo;
import gov.nasa.jpf.vm.DoubleArrayFields;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Fields;
import gov.nasa.jpf.vm.FloatArrayFields;
import gov.nasa.jpf.vm.IntArrayFields;
import gov.nasa.jpf.vm.AresVM;
import gov.nasa.jpf.vm.LongArrayFields;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ReferenceArrayFields;
import gov.nasa.jpf.vm.ShortArrayFields;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Types;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.javelus.ares.transformer.FileTransformer;

public class HeapAdapter {

    private static final boolean debug = Ares.debugHeapAdapter;

    public static final Object LOADED_FLAG = new Object();

    private static final Class<?>[] parameters = new Class[] {String.class};

    static Map<Class<?>, ObjectTransformer<?>> objectTransformers = new HashMap<Class<?>, ObjectTransformer<?>>();

    static {
        objectTransformers.put(File.class, new FileTransformer());
    }

    @SuppressWarnings("unchecked")
    public static <T> ObjectTransformer<T> getTransformer(Class<T> clazz) {
        return (ObjectTransformer<T>) objectTransformers.get(clazz);
    }

    public static String buildMethodSignature(Class<?>[] parameterTypes, Class<?> returnType) {
        StringBuilder sb = new StringBuilder();

        sb.append('(');

        for(Class<?> clazz : parameterTypes){
            sb.append(Types.getTypeSignature(clazz.getName(), false));
        }
        sb.append(')');

        if (returnType != null) {
            sb.append(Types.getTypeSignature(returnType.getName(), false));
        } else {
            sb.append('V');
        }

        return sb.toString();
    }
    public static URL findJavaResource(ClassLoader cl, String clsName) {
        Method findResourceMethod = getFindLoadedClass(cl.getClass());
        findResourceMethod.setAccessible(true);
        URL result = null;
        try {
            result = (URL) findResourceMethod.invoke(cl, clsName);
        } catch (IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            throw new UninterpretableException(e);
        }
        return result;
    }

    public static Class<?> findLoadedJavaClass(ClassLoader cl, String clsName) {
        Method findLoadedClassMethod = getFindLoadedClass(cl.getClass());
        findLoadedClassMethod.setAccessible(true);
        Class<?> result = null;
        try {
            result = (Class<?>) findLoadedClassMethod.invoke(cl, clsName);
        } catch (IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            throw new UninterpretableException(e);
        }
        return result;
    }
    /**
     * TODO use cache
     * @param clazz
     * @return
     */
    static Method getFindLoadedClass (Class<?> clazz) {
        return resolveMethod(clazz, "findLoadedClass", parameters);
    }

    static Method getFindResource (Class<?> clazz) {
        return resolveMethod(clazz, "findResource", parameters);
    }

    public static void main(String []args) {
        for(Method m:HeapAdapter.class.getDeclaredMethods()){
            System.out.println(buildMethodSignature(m.getParameterTypes(), m.getReturnType()));
        }
    }

    static Field resolveField(Class<?> clazz, String fname){
        Field field = null;
        Class<?> origin = clazz;
        while (clazz != null) {
            try {
                field = clazz.getDeclaredField(fname);
                if (field != null) {
                    return field;
                }
            } catch (NoSuchFieldException e) {
                if (clazz.getSuperclass() == null) {
                    throw new UninterpretableException("Cannot find field " + fname + " in " + origin.getName(), e); 
                }
                clazz = clazz.getSuperclass();
            } catch (SecurityException e) {
                throw new UninterpretableException(e);
            }
        }

        throw new RuntimeException("Should not reach here");
    }
    static Method resolveMethod(Class<?> clazz, String methodName, Class<?>[] params){
        Method method = null;
        Class<?> origin = clazz;
        while (clazz != null) {
            try {
                method = clazz.getDeclaredMethod(methodName, params);
                if (method != null) {
                    return method;
                }
            } catch (NoSuchMethodException e) {
                clazz = clazz.getSuperclass();
                if (clazz == null) {
                    throw new UninterpretableException("Cannot find field " + methodName + " in " + origin.getName(), e); 
                }
            } catch (SecurityException e) {
                throw new UninterpretableException(e);
            }
        }
        throw new RuntimeException("Should not reach here");
    }

    private Map<ClassLoader, ClassLoaderInfo> javaClassLoaderToJPFClassLoaderInfo = new HashMap<ClassLoader, ClassLoaderInfo>();



    private Map<Class<?>, ClassInfo> javaClassToJPFClassInfo = new HashMap<Class<?>, ClassInfo>();

    private Map<Field, FieldInfo> javaFieldToJPFFieldInfo = new HashMap<Field, FieldInfo>();

    private Map<Object, MethodInfo> javaMethodToJPFMethodInfo = new HashMap<Object, MethodInfo>();

    private Map<Object, Integer> javaObjectToJPFObject = new IdentityHashMap<Object, Integer>();

    private Map<ClassInfo, Class<?>> jpfClassInfoToJavaClass = new HashMap<ClassInfo, Class<?>>();

    private Map<ClassLoaderInfo, ClassLoader> jpfClassLoaderInfoTojavaClassLoader = new HashMap<ClassLoaderInfo, ClassLoader>();

    private Map<FieldInfo, Field> jpfFieldInfoToJavaField = new HashMap<FieldInfo, Field>();

    private Map<MethodInfo, Object> jpfMethodInfoToJavaMethod = new HashMap<MethodInfo, Object>();

    private AresVM vm;

    public HeapAdapter (AresVM vm) {
        this.vm = vm;
    }

    private ElementInfo allocateJPFObject(ThreadInfo ti, Object object) {
        ElementInfo ei = null;
        if (object instanceof String) {
            String s = (String)object;

            if (s.intern() == s) {
                ei = ti.getHeap().newInternString(s, ti);
            } else {
                ei = ti.getHeap().newString(s, ti);
                ei.setObjectAttr(object);
            }

            return ei;
        }

        if (object instanceof Class) {
            ClassInfo ci = getJPFClass(ti, (Class<?>) object);
            ei = ci.getClassObject();
            return ei;
        }

        Class<?> javaClass = object.getClass();
        ClassInfo ci = getJPFClass(ti, javaClass);

        if (javaClass.isArray()) {
            ei = ti.getHeap().newArray(Types.getArrayElementType(ci.getType()), Array.getLength(object), ti);
        } else {
            if (!isLoadedFromHostVM(ci)) {
                System.out.println("WARNNING: allocating an object of " + ci + " that has been reimplemented by JPF.");
            }
            ei = ti.getHeap().newObject(ci, ti);
        }
        ei.setObjectAttr(object);
        return ei;
    }

    public void bindStaticElementInfo(ClassInfo ci) {
        Class<?> javaClass = getJavaClass(ci);
        ci.getModifiableStaticElementInfo().setObjectAttr(javaClass);
    }

    public void clearInstanceObjects() {
        for(Entry<Object, Integer> e : javaObjectToJPFObject.entrySet()) {
            Object o = e.getKey();
            int ref = e.getValue();
            ElementInfo ei = vm.getElementInfo(ref);
            if (ei == null) {
                continue;
            }
            if (ei.getObjectAttr() == o) {
                if (ei.isFrozen()) {
                    ei.defreeze();
                    ei.setObjectAttr(null);
                    ei.freeze();
                } else {
                    ei.setObjectAttr(null);
                }
            }
        }
        javaObjectToJPFObject.clear();
    }

    public Class<?> findLoadedJavaClass(ClassInfo ci) {
        Class<?> ret = this.jpfClassInfoToJavaClass.get(ci);
        if (ret != null) {
            return ret;
        }

        ClassLoader javaClassLoader = getJavaClassLoader(ci.getClassLoaderInfo());
        return findLoadedJavaClass(javaClassLoader, ci.getName());
    }

    public boolean forceInitialized(ClassInfo ci) {
        if (!vm.isInitialized()) {
            return false;
        }
        if (isLoadedFromHostVM(ci) // loaded from host VM but haven't been loaded yet.
                && findLoadedJavaClass(ci) != null) {
            return true;
        }
        return false;
    }

    /**
     * Clear all normal objects as we have not use weakreference or softreference
     */
    public void gc() {
        List<Object> garbages = new LinkedList<Object>();
        for(Entry<Object, Integer> e : javaObjectToJPFObject.entrySet()) {
            Object o = e.getKey();
            int ref = e.getValue();
            ElementInfo ei = vm.getElementInfo(ref);
            if (ei == null) {
                garbages.add(o);
            }
        }

        for (Object key : garbages) {
            javaObjectToJPFObject.remove(key);
        }
    }

    public boolean getArrayBooleanElement(ThreadInfo ti, int arrayRef, int index) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getElementAttr(index) == LOADED_FLAG) {
            return ei.getBooleanElement(index);
        }

        boolean ret = false;
        try {
            ret = Array.getBoolean(o, index);
            ei.setBooleanElement(index, ret);
            ei.setElementAttr(index, LOADED_FLAG);
        } catch (IllegalArgumentException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public byte getArrayByteElement(ThreadInfo ti, int arrayRef, int index) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getElementAttr(index) == LOADED_FLAG) {
            return ei.getByteElement(index);
        }

        byte ret = 0;
        try {
            ret = Array.getByte(o, index);
            ei.setByteElement(index, ret);
            ei.setElementAttr(index, LOADED_FLAG);
        } catch (IllegalArgumentException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public char getArrayCharElement(ThreadInfo ti, int arrayRef, int index) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getElementAttr(index) == LOADED_FLAG) {
            return ei.getCharElement(index);
        }

        char ret = 0;
        try {
            ret = Array.getChar(o, index);
            ei.setCharElement(index, ret);
            ei.setElementAttr(index, LOADED_FLAG);
        } catch (IllegalArgumentException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public double getArrayDoubleElement(ThreadInfo ti, int arrayRef, int index) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getElementAttr(index) == LOADED_FLAG) {
            return ei.getDoubleElement(index);
        }

        double ret = 0;
        try {
            ret = Array.getDouble(o, index);
            ei.setDoubleElement(index, ret);
            ei.setElementAttr(index, LOADED_FLAG);
        } catch (IllegalArgumentException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public float getArrayFloatElement(ThreadInfo ti, int arrayRef, int index) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getElementAttr(index) == LOADED_FLAG) {
            return ei.getFloatElement(index);
        }

        float ret = 0;
        try {
            ret = Array.getFloat(o, index);
            ei.setFloatElement(index, ret);
            ei.setElementAttr(index, LOADED_FLAG);
        } catch (IllegalArgumentException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public int getArrayIntElement(ThreadInfo ti, int arrayRef, int index) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getElementAttr(index) == LOADED_FLAG) {
            return ei.getIntElement(index);
        }

        int ret = 0;
        try {
            ret = Array.getInt(o, index);
            ei.setIntElement(index, ret);
            ei.setElementAttr(index, LOADED_FLAG);
        } catch (IllegalArgumentException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public long getArrayLongElement(ThreadInfo ti, int arrayRef, int index) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getElementAttr(index) == LOADED_FLAG) {
            return ei.getLongElement(index);
        }

        long ret = 0;
        try {
            ret = Array.getLong(o, index);
            ei.setLongElement(index, ret);
            ei.setElementAttr(index, LOADED_FLAG);
        } catch (IllegalArgumentException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public int getArrayObjectElement(ThreadInfo ti, int arrayRef, int index) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getElementAttr(index) == LOADED_FLAG) {
            return ei.getReferenceElement(index);
        }

        int ret = 0;
        try {
            ret = getJPFObject(ti, Array.get(o, index));
            ei.setReferenceElement(index, ret);
            ei.setElementAttr(index, LOADED_FLAG);
        } catch (IllegalArgumentException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public short getArrayShortElement(ThreadInfo ti, int arrayRef, int index) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getElementAttr(index) == LOADED_FLAG) {
            return ei.getShortElement(index);
        }

        short ret = 0;
        try {
            ret = Array.getShort(o, index);
            ei.setShortElement(index, ret);
            ei.setElementAttr(index, LOADED_FLAG);
        } catch (IllegalArgumentException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }


    public FieldInfo getDeclaredFieldInfo (ThreadInfo ti, ElementInfo ei,
            String refType, String fname) {
        FieldInfo fi = null;

        if (ei.isObject()) {
            ClassInfo ci = ei.getClassInfo().getClassLoaderInfo().getResolvedClassInfo(refType);
            fi = ci.getDeclaredInstanceField(fname);
        } else {
            ClassInfo ci = ClassLoaderInfo.getCurrentResolvedClassInfo(refType); // <2do> should use CL argument
            fi = ci.getDeclaredStaticField(fname);
        }
        return fi;
    }


    public int getDeclaredObjectIntField(ThreadInfo ti, int objref,
            String refType, String fname) {
        ElementInfo ei = ti.getElementInfo(objref);
        Object o = ei.getObjectAttr();
        if (o == null) {
            return ei.getDeclaredIntField(refType, fname);
        }

        loadField(ti, ei, getDeclaredFieldInfo(ti, ei, refType, fname), o);

        return ei.getDeclaredIntField(refType, fname);
    }


    public FieldInfo getFieldInfo(ThreadInfo ti, int objRef, String fname) {
        ElementInfo ei = ti.getElementInfo(objRef);
        if (ei.isObject()) {
            return ei.getClassInfo().getInstanceField(fname);
        }
        return null;
    }

    public Class<?> getJavaClass(ClassInfo ci){
        DelegateClassLoaderInfo classLoaderInfo = (DelegateClassLoaderInfo) ci.getClassLoaderInfo();
        ClassLoader hostClassLoader = classLoaderInfo.getDelegationClassLoader();
        String clsName = ci.getName();

        Class<?> javaClass = null;

        try {
            javaClass = hostClassLoader.loadClass(clsName);
        } catch (ClassNotFoundException e) {
            throw new UninterpretableException(e);
        }

        return javaClass;
    }

    public ClassLoader getJavaClassLoader(ClassLoaderInfo classLoaderInfo) {
        DelegateClassLoaderInfo dcli = (DelegateClassLoaderInfo) classLoaderInfo;
        return dcli.getDelegationClassLoader();
    }

    public Field getJavaField(FieldInfo fi) {
        Field field = jpfFieldInfoToJavaField.get(fi);
        if (field == null) {
            return mapJPFFieldInfoToJavaField(fi);
        }
        return field;
    }

    public ClassInfo getJPFClass(ThreadInfo ti, Class<?> javaClass){
        ClassInfo ci = javaClassToJPFClassInfo.get(javaClass);
        if (ci == null) {
            return mapJavaClassToJPFClassInfo(ti, javaClass);
        }
        return ci;
    }

    public ClassLoaderInfo getJPFClassLoaderInfo(ThreadInfo ti,
            ClassLoader javaClassLoader) {
        ClassLoaderInfo cli = javaClassLoaderToJPFClassLoaderInfo.get(javaClassLoader);
        if (cli == null) {
            return mapJavaClassLoaderToJPFClassLoader(ti, javaClassLoader);
        }
        return cli;
    }

    public MethodInfo getJPFMethod(ThreadInfo ti, Object method) {
        MethodInfo mi = javaMethodToJPFMethodInfo.get(method);
        if (mi == null) {
            return mapJavaMethodToJPFMethodInfo(ti, method);
        }
        return mi;
    }

    public int getJPFObject(ThreadInfo ti, Object object) {
        if (object == null) {
            return MJIEnv.NULL;
        }

        Integer ret = javaObjectToJPFObject.get(object);

        if (ret == null) {
            return mapJavaObjectToJPFObject(ti, object);
        }

        if (ti.getElementInfo(ret) == null) {
            return mapJavaObjectToJPFObject(ti, object);
        }

        return ret;
    }

    public boolean getObjectBooleanField(ThreadInfo ti, int objRef, FieldInfo fi) {
        ElementInfo ei = ti.getElementInfo(objRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getFieldAttr(fi) == LOADED_FLAG) {
            return ei.getBooleanField(fi);
        }

        ei = ei.getModifiableInstance();

        Field javaField = getJavaField(fi);
        boolean ret = false;
        try {
            ret = javaField.getBoolean(o);
            ei.setBooleanField(fi, ret);
            ei.setFieldAttr(fi, LOADED_FLAG);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public boolean getObjectBooleanField(ThreadInfo ti, int objRef, String fname) {
        return getObjectBooleanField(ti, objRef, getFieldInfo(ti, objRef, fname));
    }

    public byte getObjectByteField(ThreadInfo ti, int objRef, FieldInfo fi) {
        ElementInfo ei = ti.getElementInfo(objRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getFieldAttr(fi) == LOADED_FLAG) {
            return ei.getByteField(fi);
        }

        ei = ei.getModifiableInstance();

        Field javaField = getJavaField(fi);
        byte ret = 0;
        try {
            ret = javaField.getByte(o);
            ei.setByteField(fi, ret);
            ei.setFieldAttr(fi, LOADED_FLAG);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public byte getObjectByteField(ThreadInfo ti, int objRef, String fname) {
        return getObjectByteField(ti, objRef, getFieldInfo(ti, objRef, fname));
    }

    public char getObjectCharField(ThreadInfo ti, int objRef, FieldInfo fi) {
        ElementInfo ei = ti.getElementInfo(objRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getFieldAttr(fi) == LOADED_FLAG) {
            return ei.getCharField(fi);
        }

        ei = ei.getModifiableInstance();

        Field javaField = getJavaField(fi);
        char ret = 0;
        try {
            ret = javaField.getChar(o);
            ei.setCharField(fi, ret);
            ei.setFieldAttr(fi, LOADED_FLAG);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public char getObjectCharField(ThreadInfo ti, int objRef, String fname) {
        return getObjectCharField(ti, objRef, getFieldInfo(ti, objRef, fname));
    }

    public double getObjectDoubleField(ThreadInfo ti, int objRef, FieldInfo fi) {
        ElementInfo ei = ti.getElementInfo(objRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getFieldAttr(fi) == LOADED_FLAG) {
            return ei.getDoubleField(fi);
        }

        ei = ei.getModifiableInstance();

        Field javaField = getJavaField(fi);
        double ret = 0;
        try {
            ret = javaField.getDouble(o);
            ei.setDoubleField(fi, ret);
            ei.setFieldAttr(fi, LOADED_FLAG);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public double getObjectDoubleField(ThreadInfo ti, int objRef, String fname) {
        return getObjectDoubleField(ti, objRef, getFieldInfo(ti, objRef, fname));
    }

    public float getObjectFloatField(ThreadInfo ti, int objRef, FieldInfo fi) {
        ElementInfo ei = ti.getElementInfo(objRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getFieldAttr(fi) == LOADED_FLAG) {
            return ei.getFloatField(fi);
        }

        ei = ei.getModifiableInstance();

        Field javaField = getJavaField(fi);
        float ret = 0;
        try {
            ret = javaField.getFloat(o);
            ei.setFloatField(fi, ret);
            ei.setFieldAttr(fi, LOADED_FLAG);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public float getObjectFloatField(ThreadInfo ti, int objRef, String fname) {
        return getObjectFloatField(ti, objRef, getFieldInfo(ti, objRef, fname));
    }

    public int getObjectIntField(ThreadInfo ti, int objRef, FieldInfo fi) {
        ElementInfo ei = ti.getElementInfo(objRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getFieldAttr(fi) == LOADED_FLAG) {
            return ei.getIntField(fi);
        }

        ei = ei.getModifiableInstance();

        Field javaField = getJavaField(fi);
        int ret = 0;
        try {
            ret = javaField.getInt(o);
            ei.setIntField(fi, ret);
            ei.setFieldAttr(fi, LOADED_FLAG);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public int getObjectIntField(ThreadInfo ti, int objRef, String fname) {
        return getObjectIntField(ti, objRef, getFieldInfo(ti, objRef, fname));
    }

    public long getObjectLongField(ThreadInfo ti, int objRef, FieldInfo fi) {
        ElementInfo ei = ti.getElementInfo(objRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getFieldAttr(fi) == LOADED_FLAG) {
            return ei.getLongField(fi);
        }

        ei = ei.getModifiableInstance();

        Field javaField = getJavaField(fi);
        long ret = 0;
        try {
            ret = javaField.getLong(o);
            ei.setLongField(fi, ret);
            ei.setFieldAttr(fi, LOADED_FLAG);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public long getObjectLongField(ThreadInfo ti, int objRef, String fname) {
        return getObjectLongField(ti, objRef, getFieldInfo(ti, objRef, fname));
    }

    public int getObjectObjectField(ThreadInfo ti, int objRef, FieldInfo fi) {
        ElementInfo ei = ti.getElementInfo(objRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getFieldAttr(fi) == LOADED_FLAG) {
            return ei.getReferenceField(fi);
        }

        ei = ei.getModifiableInstance();

        Field javaField = getJavaField(fi);
        int ret = 0;
        try {
            ret = getJPFObject(ti, javaField.get(o));
            ei.setReferenceField(fi, ret);
            ei.setFieldAttr(fi, LOADED_FLAG);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public int getObjectObjectField(ThreadInfo ti, int objRef, String fname) {
        return getObjectObjectField(ti, objRef, getFieldInfo(ti, objRef, fname));
    }

    public short getObjectShortField(ThreadInfo ti, int objRef, FieldInfo fi) {
        ElementInfo ei = ti.getElementInfo(objRef);
        Object o = ei.getObjectAttr();

        if (o == null || ei.getFieldAttr(fi) == LOADED_FLAG) {
            return ei.getShortField(fi);
        }

        ei = ei.getModifiableInstance();

        Field javaField = getJavaField(fi);
        short ret = 0;
        try {
            ret = javaField.getShort(o);
            ei.setShortField(fi, ret);
            ei.setFieldAttr(fi, LOADED_FLAG);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new UninterpretableException(e);
        }

        return ret;
    }

    public short getObjectShortField(ThreadInfo ti, int objRef, String fname) {
        return getObjectShortField(ti, objRef, getFieldInfo(ti, objRef, fname));
    }

    public boolean isElementLoaded(ThreadInfo ti, int objRef, int index) {
        ElementInfo ei = ti.getModifiableElementInfo(objRef);
        return ei.getElementAttr(index) != null;
    }

    public boolean isFieldLoaded(ThreadInfo ti, int objRef, FieldInfo fi) {
        ElementInfo ei = ti.getModifiableElementInfo(objRef);
        return ei.getFieldAttr(fi) != null;
    }

    public boolean isLoadedFromHostVM(ClassInfo ci) {
        if(ci.getContainer() instanceof AresClassLoaderFileContainer) {
            return true;
        }
        return false;
    }

    public ElementInfo loadArrayElementInfo(ThreadInfo ti, int objRef, Object o) {
        ElementInfo ei = ti.getModifiableElementInfo(objRef);
        Fields fields = ei.getFields();

        if (fields instanceof BooleanArrayFields) {
            for (int i = 0; i < ei.arrayLength(); i++) {
                loadBooleanArrayElement(ti, ei, i, o);
            }
        } else if (fields instanceof ByteArrayFields) {
            for (int i = 0; i < ei.arrayLength(); i++) {
                loadByteArrayElement(ti, ei, i, o);
            }
        } else if (fields instanceof CharArrayFields) {
            for (int i = 0; i < ei.arrayLength(); i++) {
                loadCharArrayElement(ti, ei, i, o);
            }
        } else if (fields instanceof DoubleArrayFields) {
            for (int i = 0; i < ei.arrayLength(); i++) {
                loadDoubleArrayElement(ti, ei, i, o);
            }
        } else if (fields instanceof FloatArrayFields) {
            for (int i = 0; i < ei.arrayLength(); i++) {
                loadFloatArrayElement(ti, ei, i, o);
            }
        } else if (fields instanceof IntArrayFields) {
            for (int i = 0; i < ei.arrayLength(); i++) {
                loadIntArrayElement(ti, ei, i, o);
            }
        } else if (fields instanceof LongArrayFields) {
            for (int i = 0; i < ei.arrayLength(); i++) {
                loadLongArrayElement(ti, ei, i, o);
            }
        } else if (fields instanceof ReferenceArrayFields) {
            for (int i = 0; i < ei.arrayLength(); i++) {
                loadObjectArrayElement(ti, ei, i, o);
            }
        } else if (fields instanceof ShortArrayFields) {
            for (int i = 0; i < ei.arrayLength(); i++) {
                loadShortArrayElement(ti, ei, i, o);
            }
        }

        return ei;
    }

    public void loadBooleanArrayElement(ThreadInfo ti, ElementInfo ei,
            int index, Object o) {
        boolean v = Array.getBoolean(o, index);
        ei.setBooleanElement(index, v);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void loadBooleanArrayElement(ThreadInfo ti, int arrayRef,
            int index) {
        ElementInfo ei = ti.getModifiableElementInfo(arrayRef);
        Object o = ei.getObjectAttr();
        loadBooleanArrayElement(ti, ei, index, o);
    }

    public void loadByteArrayElement(ThreadInfo ti, ElementInfo ei,
            int index, Object o) {
        byte v = Array.getByte(o, index);
        ei.setByteElement(index, v);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void loadByteArrayElement(ThreadInfo ti, int arrayRef,
            int index) {
        ElementInfo ei = ti.getModifiableElementInfo(arrayRef);
        Object o = ei.getObjectAttr();
        loadByteArrayElement(ti, ei, index, o);
    }


    public void loadCharArrayElement(ThreadInfo ti, ElementInfo ei,
            int index, Object o) {
        char v = Array.getChar(o, index);
        ei.setCharElement(index, v);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void loadCharArrayElement(ThreadInfo ti, int arrayRef,
            int index) {
        ElementInfo ei = ti.getModifiableElementInfo(arrayRef);
        Object o = ei.getObjectAttr();
        loadCharArrayElement(ti, ei, index, o);
    }

    public void loadDoubleArrayElement(ThreadInfo ti, ElementInfo ei,
            int index, Object o) {
        double v = Array.getDouble(o, index);
        ei.setDoubleElement(index, v);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void loadDoubleArrayElement(ThreadInfo ti, int arrayRef,
            int index) {
        ElementInfo ei = ti.getModifiableElementInfo(arrayRef);
        Object o = ei.getObjectAttr();
        loadDoubleArrayElement(ti, ei, index, o);
    }

    public ElementInfo loadElementInfo(ThreadInfo ti, int objRef) {
        ElementInfo ei = ti.getElementInfo(objRef);
        Object o = ei.getObjectAttr();
        if (o != null) {
            if (ei.isArray()) {
                ei = loadArrayElementInfo(ti, objRef, o);
            } else if (ei.isObject()) {
                ei = loadInstanceElementInfo(ti, objRef, o);
            } else {
                //loadStaticElementInfo(ti, objRef, o);
                throw new RuntimeException("Should not reach here");
            }

            // TODO We loaded all fields, we unbound it.
            ei.removeObjectAttr(o);
        }

        return ei;
    }

    public void loadField(ThreadInfo ti, ElementInfo ei, FieldInfo fi, Object o) {
        if (ei.getFieldAttr(fi) == LOADED_FLAG) {
            throw new RuntimeException("Duplicated loading");
        }

        Field javaField = getJavaField(fi);
        try {
            if (fi.isBooleanField()) {
                ei.setBooleanField(fi, javaField.getBoolean(o));
            } else if (fi.isCharField()) {
                ei.setCharField(fi, javaField.getChar(o));
            } else if (fi.isByteField()) {
                ei.setByteField(fi, javaField.getByte(o));
            } else if (fi.isShortField()) {
                ei.setShortField(fi, javaField.getShort(o));
            } else if (fi.isIntField()) {
                ei.setIntField(fi, javaField.getInt(o));
            } else if (fi.isFloatField()) {
                ei.setFloatField(fi, javaField.getFloat(o));
            } else if (fi.isDoubleField()) {
                ei.setDoubleField(fi, javaField.getDouble(o));
            } else if (fi.isLongField()) {
                ei.setLongField(fi, javaField.getLong(o));
            } else if (fi.isReference()) {
                Object f = javaField.get(o);
                ei.setReferenceField(fi, getJPFObject(ti, f));
                if (f != null && ti.getElementInfo(ei.getReferenceField(fi)) == null) {
                    throw new RuntimeException("Object has been collected!");
                }
            }
            ei.setFieldAttr(fi, LOADED_FLAG);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new UninterpretableException(e);
        }
    }

    public void loadFloatArrayElement(ThreadInfo ti, ElementInfo ei,
            int index, Object o) {
        float v = Array.getFloat(o, index);
        ei.setFloatElement(index, v);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void loadFloatArrayElement(ThreadInfo ti, int arrayRef,
            int index) {
        ElementInfo ei = ti.getModifiableElementInfo(arrayRef);
        Object o = ei.getObjectAttr();
        loadFloatArrayElement(ti, ei, index, o);
    }

    public ElementInfo loadInstanceElementInfo(ThreadInfo ti, int objRef, Object o) {
        ElementInfo ei = ti.getModifiableElementInfo(objRef);
        for (int i=0; i < ei.getNumberOfFields(); i++) {
            FieldInfo fi = ei.getFieldInfo(i);
            if (ei.getFieldAttr(fi) != LOADED_FLAG) {
                loadField(ti, ei, fi, o);
            }
        }
        return ei;
    }

    public void loadInstanceField(ThreadInfo ti, int objRef, FieldInfo fi) {
        ElementInfo ei = ti.getModifiableElementInfo(objRef);
        Object o = ei.getObjectAttr();
        if (ei.getFieldAttr(fi) == LOADED_FLAG) {
            throw new RuntimeException("Double loaded!");
        }
        loadField(ti, ei, fi, o);
    }

    public void loadIntArrayElement(ThreadInfo ti, ElementInfo ei,
            int index, Object o) {
        int v = Array.getInt(o, index);
        ei.setIntElement(index, v);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void loadIntArrayElement(ThreadInfo ti, int arrayRef,
            int index) {
        ElementInfo ei = ti.getModifiableElementInfo(arrayRef);
        Object o = ei.getObjectAttr();
        loadIntArrayElement(ti, ei, index, o);
    }

    public void loadLongArrayElement(ThreadInfo ti, ElementInfo ei,
            int index, Object o) {
        long v = Array.getLong(o, index);
        ei.setLongElement(index, v);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void loadLongArrayElement(ThreadInfo ti, int arrayRef,
            int index) {
        ElementInfo ei = ti.getModifiableElementInfo(arrayRef);
        Object o = ei.getObjectAttr();
        loadLongArrayElement(ti, ei, index, o);
    }

    public void /*int*/ loadObjectArrayElement(ThreadInfo ti, ElementInfo ei,
            int index, Object o) {
        Object e = Array.get(o, index);
        int ref = getJPFObject(ti, e);
        ei.setReferenceElement(index, ref);
        ei.setElementAttr(index, LOADED_FLAG);
        //return ref;
    }

    public void /*int*/ loadObjectArrayElement(ThreadInfo ti, int arrayRef,
            int index) {
        ElementInfo ei = ti.getModifiableElementInfo(arrayRef);
        Object o = ei.getObjectAttr();
        loadObjectArrayElement(ti, ei, index, o);
        //return ref;
    }

    public void loadShortArrayElement(ThreadInfo ti, ElementInfo ei,
            int index, Object o) {
        short v = Array.getShort(o, index);
        ei.setShortElement(index, v);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void loadShortArrayElement(ThreadInfo ti, int arrayRef,
            int index) {
        ElementInfo ei = ti.getModifiableElementInfo(arrayRef);
        Object o = ei.getObjectAttr();
        loadShortArrayElement(ti, ei, index, o);
    }

    public void loadStaticElementInfo(ThreadInfo ti, ElementInfo ei, Object o) {
        for (int i=0; i < ei.getNumberOfFields(); i++) {
            FieldInfo fi = ei.getFieldInfo(i);
            if (ei.getFieldAttr(fi) != LOADED_FLAG) {
                loadField(ti, ei, fi, o);
            }
        }
    }

    public void loadStaticField(ThreadInfo ti, ClassInfo ci, FieldInfo fi) {
        ElementInfo ei = ci.getModifiableStaticElementInfo();
        Object /*Class<?>*/ javaClass = ei.getObjectAttr();//findLoadedJavaClass(ci);
        if (javaClass != null) {
            loadField(ti, ei, fi, javaClass);
        } else {
            //ei.setFieldAttr(fi, loadedFlag);
            throw new RuntimeException("Sanity check failed!");
        }

    }

    /*	public void loadStaticField(ThreadInfo ti, ClassInfo ci, String fname) {
		ElementInfo ei = ci.getStaticElementInfo();
		Object o = ei.getObjectAttr();
		if (o != null) {
			FieldInfo fi = ci.getStaticField(fname);
			loadField(ti, ei, fi, o);
		}
	}*/

    public ElementInfo loadStaticField(ThreadInfo ti, ElementInfo ei, FieldInfo fi) {
        Object o = ei.getObjectAttr();

        if (o != null && ei.getFieldAttr(fi) != LOADED_FLAG) {
            ei = ei.getModifiableInstance();
            loadField(ti, ei, fi, o);
            return ei;
        }

        return ei;
    }

    private ClassLoaderInfo mapJavaClassLoaderToJPFClassLoader(ThreadInfo ti,
            ClassLoader javaClassLoader) {
        if (ClassLoader.getSystemClassLoader() == javaClassLoader) {
            return ClassLoaderInfo.getCurrentSystemClassLoader();
        } else if (javaClassLoader == null) {
            return ClassLoaderInfo.getCurrentSystemClassLoader();
        }

        int loaderRef = getJPFObject(ti, javaClassLoader);
        ClassLoader parent = javaClassLoader.getParent();

        ClassLoaderInfo parentInfo = getJPFClassLoaderInfo(ti, parent);

        AresClassLoaderInfo classLoaderInfo = new AresClassLoaderInfo(vm, loaderRef, parentInfo, javaClassLoader);

        javaClassLoaderToJPFClassLoaderInfo.put(javaClassLoader, classLoaderInfo);
        jpfClassLoaderInfoTojavaClassLoader.put(classLoaderInfo, javaClassLoader);

        return classLoaderInfo;
    }

    private ClassInfo mapJavaClassToJPFClassInfo(ThreadInfo ti,
            Class<?> javaClass) {
        ClassLoader javaClassLoader = javaClass.getClassLoader();

        //		if (debug) {
        //			System.out.println("Mapping " + javaClass.getName() + " from " + javaClassLoader + " to JPFClass");
        //		}


        ClassLoaderInfo cli = getJPFClassLoaderInfo(ti, javaClassLoader);
        String className = javaClass.getName();

        //		if (debug) {
        //			System.out.println("Mapping " + javaClass.getName() + " from " + javaClassLoader + " to JPFClass");
        //		}

        ClassInfo ci = cli.loadClass(className); 

        if (!isLoadedFromHostVM(ci)) {
            if (debug) {
                System.out.println("Mapping class in JPF will result in LOSS_SYNC error.");
            }
        } else {
            ci.registerClass(ti);
        }

        jpfClassInfoToJavaClass.put(ci, javaClass);
        javaClassToJPFClassInfo.put(javaClass, ci);

        return ci; 
    }

    private MethodInfo mapJavaMethodToJPFMethodInfo(ThreadInfo ti, Object object) {
        MethodInfo mi = null;
        ClassInfo ci = null;

        if (object instanceof Method) {
            Method method = (Method) object;
            ci = getJPFClass(ti, method.getDeclaringClass());
            mi = ci.getMethod(method.getName(), buildMethodSignature(method.getParameterTypes(),method.getReturnType()), false);
        } else if (object instanceof Constructor) {
            Constructor<?> method = (Constructor<?>) object;
            ci = getJPFClass(ti, method.getDeclaringClass());
            int modifier = method.getModifiers();
            String methodName = "<init>";
            if (Modifier.isStatic(modifier)) {
                methodName = "<clinit>";
                System.out.println("WARNNING: get <clinit> of " + ci.getName() + " from Java method.");
            }
            mi = ci.getMethod(methodName, buildMethodSignature(method.getParameterTypes(), null), false);
        }

        if (mi == null) {
            throw new RuntimeException("Cannot find method " + object);
        }

        jpfMethodInfoToJavaMethod.put(mi, object);
        javaMethodToJPFMethodInfo.put(object, mi);

        return mi;
    }

    private int mapJavaObjectToJPFObject(ThreadInfo ti, Object object) {
        ElementInfo ei = allocateJPFObject(ti, object);
        javaObjectToJPFObject.put(object, ei.getObjectRef());

        transformObject(ti, ei, object);

        return ei.getObjectRef();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected void transformObject(ThreadInfo ti, ElementInfo ei, Object object) {
        ObjectTransformer transformer = getTransformer(object.getClass());
        if (transformer != null) {
            transformer.transform(ti, this, ei, object);
        }
    }

    private Field mapJPFFieldInfoToJavaField(FieldInfo fi) {
        Class<?> javaClass = getJavaClass(fi.getClassInfo());
        Field javaField = null;

        javaField = resolveField(javaClass, fi.getName());
        javaField.setAccessible(true);

        jpfFieldInfoToJavaField.put(fi, javaField);
        javaFieldToJPFFieldInfo.put(javaField, fi);

        return javaField;
    }

    public void setArrayBooleanElement(ThreadInfo ti, int arrayRef, int index, boolean newValue) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        ei.setBooleanElement(index, newValue);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void setArrayByteElement(ThreadInfo ti, int arrayRef, int index, byte newValue) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        ei.setByteElement(index, newValue);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void setArrayCharElement(ThreadInfo ti, int arrayRef, int index, char newValue) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        ei.setCharElement(index, newValue);
        ei.setElementAttr(index, LOADED_FLAG);
    }


    public void setArrayDoubleElement(ThreadInfo ti, int arrayRef, int index, double newValue) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        ei.setDoubleElement(index, newValue);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void setArrayFloatElement(ThreadInfo ti, int arrayRef, int index, float newValue) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        ei.setFloatElement(index, newValue);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void setArrayIntElement(ThreadInfo ti, int arrayRef, int index, int newValue) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        ei.setIntElement(index, newValue);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void setArrayLongElement(ThreadInfo ti, int arrayRef, int index, long newValue) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        ei.setLongElement(index, newValue);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void setArrayObjectElement(ThreadInfo ti, int arrayRef, int index, int newValue) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        ei.setReferenceElement(index, newValue);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void setArrayShortElement(ThreadInfo ti, int arrayRef, int index, short newValue) {
        ElementInfo ei = ti.getElementInfo(arrayRef);
        ei.setShortElement(index, newValue);
        ei.setElementAttr(index, LOADED_FLAG);
    }

    public void setDeclaredObjectField(ThreadInfo ti, int objref,
            String refType, String fname, int val) {
        ElementInfo ei = ti.getElementInfo(objref);
        Object o = ei.getObjectAttr();
        if (o == null) {
            ei.setDeclaredReferenceField(fname, refType, val);
            return;
        }

        setObjectObjectField(ti, objref, getDeclaredFieldInfo(ti, ei, refType, fname), val);
    }

    public void setObjectBooleanField(ThreadInfo ti, int ref, FieldInfo fi, boolean newValue) {
        ElementInfo ei = ti.getModifiableElementInfo(ref);
        ei.setBooleanField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

    public void setObjectBooleanField(ThreadInfo ti, int ref, String fname, boolean newValue) {
        setObjectBooleanField(ti, ref, getFieldInfo(ti, ref, fname), newValue);
    }

    public void setObjectByteField(ThreadInfo ti, int ref, FieldInfo fi, byte newValue) {
        ElementInfo ei = ti.getModifiableElementInfo(ref);
        ei.setByteField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

    public void setObjectByteField(ThreadInfo ti, int ref, String fname, byte newValue) {
        setObjectByteField(ti, ref, getFieldInfo(ti, ref, fname), newValue);
    }

    public void setObjectCharField(ThreadInfo ti, int ref, FieldInfo fi, char newValue) {
        ElementInfo ei = ti.getModifiableElementInfo(ref);
        ei.setCharField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

    public void setObjectCharField(ThreadInfo ti, int ref, String fname, char newValue) {
        setObjectCharField(ti, ref, getFieldInfo(ti, ref, fname), newValue);
    }

    public void setObjectDoubleField(ThreadInfo ti, int ref, FieldInfo fi, double newValue) {
        ElementInfo ei = ti.getModifiableElementInfo(ref);
        ei.setDoubleField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

    public void setObjectDoubleField(ThreadInfo ti, int ref, String fname, double newValue) {
        setObjectDoubleField(ti, ref, getFieldInfo(ti, ref, fname), newValue);
    }

    public void setObjectFloatField(ThreadInfo ti, int ref, FieldInfo fi, float newValue) {
        ElementInfo ei = ti.getModifiableElementInfo(ref);
        ei.setFloatField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }
    public void setObjectFloatField(ThreadInfo ti, int ref, String fname, float newValue) {
        setObjectFloatField(ti, ref, getFieldInfo(ti, ref, fname), newValue);
    }

    public void setObjectIntField(ThreadInfo ti, int ref, FieldInfo fi, int newValue) {
        ElementInfo ei = ti.getModifiableElementInfo(ref);
        ei.setIntField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

    public void setObjectIntField(ThreadInfo ti, int ref, String fname, int newValue) {
        setObjectIntField(ti, ref, getFieldInfo(ti, ref, fname), newValue);
    }

    public void setObjectLongField(ThreadInfo ti, int ref, FieldInfo fi, long newValue) {
        ElementInfo ei = ti.getModifiableElementInfo(ref);
        ei.setLongField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

    public void setObjectLongField(ThreadInfo ti, int ref, String fname, long newValue) {
        setObjectLongField(ti, ref, getFieldInfo(ti, ref, fname), newValue);
    }

    public void setObjectObjectField(ThreadInfo ti, int ref, FieldInfo fi, int newValue) {
        ElementInfo ei = ti.getModifiableElementInfo(ref);
        ei.setReferenceField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

    public void setObjectObjectField(ThreadInfo ti, int ref, String fname, int newValue) {
        setObjectObjectField(ti, ref, getFieldInfo(ti, ref, fname), newValue);
    }

    public void setObjectShortField(ThreadInfo ti, int ref, FieldInfo fi, short newValue) {
        ElementInfo ei = ti.getModifiableElementInfo(ref);
        ei.setShortField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }


    public void setObjectShortField(ThreadInfo ti, int ref, String fname, short newValue) {
        setObjectShortField(ti, ref, getFieldInfo(ti, ref, fname), newValue);
    }

    public void loadStaticElementInfo(ThreadInfo ti, ClassInfo ci) {
        ElementInfo cei = ci.getStaticElementInfo();
        Object o = cei.getObjectAttr();
        if (o != null) {
            loadStaticElementInfo(ti, cei, o);
        }
    }

    public void setObjectBooleanField(ThreadInfo ti, ElementInfo ei, FieldInfo fi, boolean newValue) {
        ei.setBooleanField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

    public void setObjectByteField(ThreadInfo ti, ElementInfo ei, FieldInfo fi, byte newValue) {
        ei.setByteField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

    public void setObjectCharField(ThreadInfo ti, ElementInfo ei, FieldInfo fi, char newValue) {
        ei.setCharField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

    public void setObjectDoubleField(ThreadInfo ti, ElementInfo ei, FieldInfo fi, double newValue) {
        ei.setDoubleField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

    public void setObjectFloatField(ThreadInfo ti, ElementInfo ei, FieldInfo fi, float newValue) {
        ei.setFloatField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

    public void setObjectIntField(ThreadInfo ti, ElementInfo ei, FieldInfo fi, int newValue) {
        ei.setIntField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

    public void setObjectLongField(ThreadInfo ti, ElementInfo ei, FieldInfo fi, long newValue) {
        ei.setLongField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

    public void setObjectObjectField(ThreadInfo ti, ElementInfo ei, FieldInfo fi, int newValue) {
        ei.setReferenceField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

    public void setObjectShortField(ThreadInfo ti, ElementInfo ei, FieldInfo fi, short newValue) {
        ei.setShortField(fi, newValue);
        ei.setFieldAttr(fi, LOADED_FLAG);
    }

}