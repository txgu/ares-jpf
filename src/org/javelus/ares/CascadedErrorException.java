package org.javelus.ares;

import gov.nasa.jpf.vm.ClassInfo;

public class CascadedErrorException extends AresException {

    /**
     * 
     */
    private static final long serialVersionUID = 1749299459600945246L;

    private ClassInfo ci;

    public CascadedErrorException() {
        super();
    }

    public CascadedErrorException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public CascadedErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public CascadedErrorException(String message) {
        super(message);
    }

    public CascadedErrorException(Throwable cause) {
        super(cause);
    }

    public void setExceptionClassInfo(ClassInfo ci) {
        this.ci = ci;
    }

    public ClassInfo getExceptionClassInfo() {
        return ci;
    }
}
