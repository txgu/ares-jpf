package org.javelus.ares;

import gov.nasa.jpf.Ares;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;


public class StackFrameDesc {
    public final Object method;

    public final int pc;

    public final long[] longSlots;
    public final Object[] objectSlots;


    public StackFrameDesc(Object method, int pc, long[] locals, Object[] localsOop) {
        this.method = method;
        this.pc = pc;
        this.longSlots = locals;
        this.objectSlots = localsOop;
    }



    public String toString() {
        StringBuilder sb = new StringBuilder();

        MethodInfo mi = Ares.getHeapAdapter().getJPFMethod(ThreadInfo.getCurrentThread(), method);

        if (mi == null) {
            return method.toString();
        }

        int nof_locals = mi.getMaxLocals();
        int nof_stack = mi.getMaxStack();

        sb.append(method);
        sb.append("\npc:\t");
        sb.append(pc);


        sb.append("\tmax_locals:\t");
        sb.append(nof_locals);
        sb.append("\tmax_stack:\t");
        sb.append(nof_stack);
        sb.append("\nLocals:\t");

        for (int i = 0;i<longSlots.length;i++) {
            if (i == nof_locals) {
                sb.append("\nStack:\t");
            }

            long v = longSlots[i];
            Object o = objectSlots[i];
            if (o == null) {
                sb.append(v);
            } else {
                sb.append(o.getClass());
            }

            sb.append(",");
        }

        return sb.toString();
    }
}
