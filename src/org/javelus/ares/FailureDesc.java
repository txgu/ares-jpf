package org.javelus.ares;

import gov.nasa.jpf.Ares;

public class FailureDesc {
    public static final boolean debug = Ares.DebugFailureDesc;

    Object causeEception;
    StackFrameDesc[] stackFrames;


    public FailureDesc(Object causeEception, StackFrameDesc[] stackFrames) {
        this.causeEception = causeEception;
        this.stackFrames = stackFrames;
    }


    public Object getCauseEception() {
        return causeEception;
    }


    public StackFrameDesc[] getStackFrames() {
        return stackFrames;
    }

    public static FailureDesc buildFailureDesc(Object[] data) {
        if (debug) {
            System.out.println("Parsing data, length=" + data.length);
        }

        int last = data.length-1;
        Object exception = data[last--];

        if (debug) {
            System.out.println("Pending exception: " + exception);
        }

        int[] pcs = (int[])data[last--];

        int frameLength = pcs.length;

        if (debug) {
            System.out.println("Stack length: " + frameLength);
        }

        StackFrameDesc[] descs = new StackFrameDesc[frameLength];

        int j = 0;
        for (int i=0; i<frameLength; i++) {
            Object method = data[j++];
            long[] longSlots = (long[]) data[j++];
            Object[] objectSlots = (Object[]) data[j++];
            descs[i] = new StackFrameDesc(method, pcs[i], longSlots, objectSlots);
            if (debug) {
                System.out.println("[" + i + "]\n" + descs[i]);
            }
        }

        return new FailureDesc(exception, descs);
    }
}
