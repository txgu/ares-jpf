package gov.nasa.jpf.vm;

import gov.nasa.jpf.Ares;
import gov.nasa.jpf.jvm.JVMStackFrame;
import gov.nasa.jpf.util.FixedBitSet;

import java.io.PrintWriter;

import org.javelus.ares.HeapAdapter;
import org.javelus.ares.InternalErrorException;
import org.javelus.ares.StackFrameDesc;
import org.javelus.ares.UninterpretableException;

public class AresStackFrameAdapter64 extends JVMStackFrame implements AresHostStackFrame64 {

    private static final boolean debug = Ares.debugStackFrame;

    protected FixedBitSet isLoaded; // which slots has been loaded from host VM

    private long[] longSlots;

    public AresStackFrameAdapter64(MethodInfo callee) {
        super(callee);

        isLoaded = isRef.clone();
    }

    public AresStackFrameAdapter64 clone() {
        AresStackFrameAdapter64 sf = (AresStackFrameAdapter64) super.clone();

        sf.longSlots = longSlots.clone();
        sf.isLoaded = isLoaded.clone();

        return sf;
    }

    @Override
    public void dup() {
        // .. A =>
        // .. A A
        // ^

        int t = top;

        int td = t + 1;
        slots[td] = slots[t];
        longSlots[td] = longSlots[t];
        isRef.set(td, isRef.get(t));
        isLoaded.set(td, isLoaded.get(t));

        if (attrs != null) {
            attrs[td] = attrs[t];
        }

        top = td;
    }

    @Override
    public void dup_x1() {
        // .. A B =>
        // .. B A B
        // ^

        int b;
        boolean bRef;
        long lb;
        boolean bLoaded;
        Object bAnn = null;
        int ts, td;
        int t = top;

        // duplicate B
        ts = t;
        td = t + 1;
        slots[td] = b = slots[ts];
        longSlots[td] = lb = longSlots[ts];
        bRef = isRef.get(ts);
        bLoaded = isLoaded.get(ts);
        isRef.set(td, bRef);
        isLoaded.set(td, bLoaded);
        if (attrs != null) {
            attrs[td] = bAnn = attrs[ts];
        }

        // shuffle A
        ts--;
        td = t; // ts=top-1, td = top
        slots[td] = slots[ts];
        longSlots[td] = longSlots[ts];
        isRef.set(td, isRef.get(ts));
        isLoaded.set(td, isLoaded.get(ts));
        if (attrs != null) {
            attrs[td] = attrs[ts];
        }

        // shuffle B
        td = ts; // td=top-1
        slots[td] = b;
        longSlots[td] = lb;
        isRef.set(td, bRef);
        isLoaded.set(td, bLoaded);
        if (attrs != null) {
            attrs[td] = bAnn;
        }

        top++;
    }

    @Override
    public void dup_x2() {
        // .. A B C =>
        // .. C A B C
        // ^

        int c;
        long lc;
        boolean cRef;
        boolean cLoaded;
        Object cAnn = null;
        int ts, td;
        int t = top;

        // duplicate C
        ts = t;
        td = t + 1;
        slots[td] = c = slots[ts];
        longSlots[td] = lc = longSlots[ts];
        cRef = isRef.get(ts);
        cLoaded = isLoaded.get(ts);
        isRef.set(td, cRef);
        isLoaded.set(td, cLoaded);
        if (attrs != null) {
            attrs[td] = cAnn = attrs[ts];
        }

        // shuffle B
        td = ts;
        ts--; // td=top, ts=top-1
        slots[td] = slots[ts];
        longSlots[td] = longSlots[ts];
        isRef.set(td, isRef.get(ts));
        isLoaded.set(td, isLoaded.get(ts));
        if (attrs != null) {
            attrs[td] = attrs[ts];
        }

        // shuffle A
        td = ts;
        ts--; // td=top-1, ts=top-2
        slots[td] = slots[ts];
        longSlots[td] = longSlots[ts];
        isRef.set(td, isRef.get(ts));
        isLoaded.set(td, isLoaded.get(ts));
        if (attrs != null) {
            attrs[td] = attrs[ts];
        }

        // shuffle C
        td = ts; // td = top-2
        slots[td] = c;
        longSlots[td] = lc;
        isRef.set(td, cRef);
        isLoaded.set(td, cLoaded);

        if (attrs != null) {
            attrs[td] = cAnn;
        }

        top++;
    }

    @Override
    public void dup2() {
        // .. A B =>
        // .. A B A B
        // ^

        int ts, td;
        int t = top;

        // duplicate A
        td = t + 1;
        ts = t - 1;
        slots[td] = slots[ts];
        longSlots[td] = longSlots[ts];
        isRef.set(td, isRef.get(ts));
        isLoaded.set(td, isLoaded.get(ts));
        if (attrs != null) {
            attrs[td] = attrs[ts];
        }

        // duplicate B
        td++;
        ts = t;
        slots[td] = slots[ts];
        longSlots[td] = longSlots[ts];
        isRef.set(td, isRef.get(ts));
        isLoaded.set(td, isLoaded.get(ts));
        if (attrs != null) {
            attrs[td] = attrs[ts];
        }

        top = td;
    }

    @Override
    public void dup2_x1() {
        // .. A B C =>
        // .. B C A B C
        // ^

        int b, c;
        long lb, lc;
        boolean bRef, cRef;
        boolean bLoaded, cLoaded;
        Object bAnn = null, cAnn = null;
        int ts, td;
        int t = top;

        // duplicate C
        ts = t;
        td = t + 2; // ts=top, td=top+2
        slots[td] = c = slots[ts];
        longSlots[td] = lc = longSlots[ts];
        cRef = isRef.get(ts);
        cLoaded = isLoaded.get(ts);
        isRef.set(td, cRef);
        isLoaded.set(td, cLoaded);
        if (attrs != null) {
            attrs[td] = cAnn = attrs[ts];
        }

        // duplicate B
        ts--;
        td--; // ts=top-1, td=top+1
        slots[td] = b = slots[ts];
        longSlots[td] = lb = longSlots[ts];
        bRef = isRef.get(ts);
        bLoaded = isLoaded.get(ts);
        isRef.set(td, bRef);
        isLoaded.set(td, bLoaded);
        if (attrs != null) {
            attrs[td] = bAnn = attrs[ts];
        }

        // shuffle A
        ts = t - 2;
        td = t; // ts=top-2, td=top
        slots[td] = slots[ts];
        longSlots[td] = longSlots[ts];
        isRef.set(td, isRef.get(ts));
        isLoaded.set(td, isLoaded.get(ts));
        if (attrs != null) {
            attrs[td] = attrs[ts];
        }

        // shuffle B
        td = ts; // td=top-2
        slots[td] = b;
        longSlots[td] = lb;
        isRef.set(td, bRef);
        isLoaded.set(td, bLoaded);
        if (attrs != null) {
            attrs[td] = bAnn;
        }

        // shuffle C
        td++; // td=top-1
        slots[td] = c;
        longSlots[td] = lc;
        isLoaded.set(td, cLoaded);
        if (attrs != null) {
            attrs[td] = cAnn;
        }

        top += 2;
    }

    @Override
    public void dup2_x2() {
        // .. A B C D =>
        // .. C D A B C D
        // ^

        int c, d;
        long lc, ld;
        boolean cRef, dRef;
        boolean cLoaded, dLoaded;
        Object cAnn = null, dAnn = null;
        int ts, td;
        int t = top;

        // duplicate C
        ts = t - 1;
        td = t + 1; // ts=top-1, td=top+1
        slots[td] = c = slots[ts];
        longSlots[td] = lc = longSlots[ts];
        cRef = isRef.get(ts);
        cLoaded = isLoaded.get(ts);
        isLoaded.set(td, cLoaded);
        if (attrs != null) {
            attrs[td] = cAnn = attrs[ts];
        }

        // duplicate D
        ts = t;
        td++; // ts=top, td=top+2
        slots[td] = d = slots[ts];
        longSlots[td] = ld = longSlots[ts];
        dRef = isRef.get(ts);
        dLoaded = isLoaded.get(ts);
        isRef.set(td, dRef);
        isLoaded.set(td, dLoaded);
        if (attrs != null) {
            attrs[td] = dAnn = attrs[ts];
        }

        // shuffle A
        ts = t - 3;
        td = t - 1; // ts=top-3, td=top-1
        slots[td] = slots[ts];
        longSlots[td] = longSlots[ts];
        isRef.set(td, isRef.get(ts));
        isLoaded.set(td, isLoaded.get(ts));
        if (attrs != null) {
            attrs[td] = attrs[ts];
        }

        // shuffle B
        ts++;
        td = t; // ts = top-2
        slots[td] = slots[ts];
        longSlots[td] = longSlots[ts];
        isRef.set(td, isRef.get(ts));
        isLoaded.set(td, isLoaded.get(ts));
        if (attrs != null) {
            attrs[td] = attrs[ts];
        }

        // shuffle D
        td = ts; // td = top-2
        slots[td] = d;
        longSlots[td] = ld;
        isRef.set(td, dRef);
        isLoaded.set(td, dLoaded);
        if (attrs != null) {
            attrs[td] = dAnn;
        }

        // shuffle C
        td--; // td = top-3
        slots[td] = c;
        longSlots[td] = lc;
        isRef.set(td, cRef);
        isLoaded.set(td, cLoaded);
        if (attrs != null) {
            attrs[td] = cAnn;
        }

        top += 2;
    }

    public double getDoubleLocalVariable(int idx) {
        if (!isLoaded.get(idx)) {
            loadDoubleAt(idx);
        }

        return Types.intsToDouble(slots[idx + 1], slots[idx]);

    }

    public float getFloatLocalVariable(int idx) {
        if (!isLoaded.get(idx)) {
            loadFloatAt(idx);
        }

        int bits = slots[idx];
        return Float.intBitsToFloat(bits);
    }

    public Object getLocalValueObject (LocalVarInfo lv) {
        if (lv != null) { // might not have been compiled with debug info
            String sig = lv.getSignature();
            int slotIdx = lv.getSlotIndex();

            if (isLoaded.get(slotIdx)) {
                int v = slots[slotIdx];

                switch (sig.charAt(0)) {
                case 'Z':
                    return Boolean.valueOf(v != 0);
                case 'B':
                    return new Byte((byte) v);
                case 'C':
                    return new Character((char) v);
                case 'S':
                    return new Short((short) v);
                case 'I':
                    return new Integer((int) v);
                case 'J':
                    return new Long(Types.intsToLong(slots[slotIdx + 1], v)); // Java is big endian, Types expects low,high
                case 'F':
                    return new Float(Float.intBitsToFloat(v));
                case 'D':
                    return new Double(Double.longBitsToDouble(Types.intsToLong(slots[slotIdx + 1], v)));
                default:  // reference
                    if (v >= 0) {
                        return VM.getVM().getHeap().get(v);
                    }
                }
            } else {
                long v = longSlots[slotIdx];

                switch (sig.charAt(0)) {
                case 'Z':
                    return Boolean.valueOf(v != 0);
                case 'B':
                    return new Byte((byte) v);
                case 'C':
                    return new Character((char) v);
                case 'S':
                    return new Short((short) v);
                case 'I':
                    return new Integer((int) v);
                case 'J':
                    return new Long(longSlots[slotIdx+1]); // Java is big endian, Types expects low,high
                case 'F':
                    return new Float(Float.intBitsToFloat((int)v));
                case 'D':
                    return new Double(Double.longBitsToDouble(longSlots[slotIdx+1]));
                default:  // reference
                    if (v >= 0) {
                        return VM.getVM().getHeap().get((int)v);
                    }
                }
            }
        }

        return null;
    }

    public int getLocalVariable (int i) {
        if (!isLoaded.get(i)) {
            loadIntAt(i);
        }

        return slots[i];
    }

    public long getLongLocalVariable(int idx) {
        if (!isLoaded.get(idx)) {
            loadLongAt(idx);
        }
        return Types.intsToLong(slots[idx + 1], slots[idx]);
    }

    public int getSlot(int idx) {
        if (!isLoaded.get(idx)) {
            return slots[idx];
        }
        throw new RuntimeException();
    }

    public int[] getSlots() {
        for (int i=0; i<=top; i++) {
            if (!isLoaded.get(i)) {
                System.out.println(this);
                new Throwable().printStackTrace();
                throw new RuntimeException("Slot[" + i + "] is not loaded");
            }
        }

        return slots;
    }

    private void loadDoubleAt(int idx) {
        long l = longSlots[idx+1];

        slots[idx] = Types.hiLong(l);
        isRef.clear(idx);
        isLoaded.set(idx);

        idx++;
        slots[idx] = Types.loLong(l);
        isRef.clear(idx);
        isLoaded.set(idx);
    }

    private void loadFloatAt(int idx) {
        slots[idx] = (int) longSlots[idx];
        isLoaded.set(idx);
    }

    private void loadIntAt(int idx) {
        slots[idx] = (int) longSlots[idx];
        isLoaded.set(idx);
    }

    private void loadLongAt(int idx) {
        long l = longSlots[idx+1];

        slots[idx] = Types.hiLong(l);
        isRef.clear(idx);
        isLoaded.set(idx);

        idx++;
        slots[idx] = Types.loLong(l);
        isRef.clear(idx);
        isLoaded.set(idx);
    }

    public int peek () {
        if (!isLoaded.get(top)) {
            loadIntAt(top);
        }
        return slots[top];
    }

    public int peek (int offset) {
        int index = top-offset;
        if (!isLoaded.get(index)) {
            loadIntAt(index);
        }
        return slots[index];
    }

    public double peekDouble() {
        int i = top;
        if (!isLoaded.get(i)) {
            loadDoubleAt(i-1);
        }
        return Types.intsToDouble( slots[i], slots[i-1]);
    }

    public double peekDouble (int offset){
        int i = top-offset;
        if (!isLoaded.get(i)) {
            loadDoubleAt(i-1);
        }
        return Types.intsToDouble( slots[i], slots[i-1]);
    }

    public float peekFloat() {
        if (!isLoaded.get(top)) {
            loadFloatAt(top);
        }
        return Float.intBitsToFloat(slots[top]);
    }

    public float peekFloat (int offset){
        int index = top-offset;
        if (!isLoaded.get(index)) {
            loadFloatAt(index);
        }
        return Float.intBitsToFloat(slots[index]);
    }

    public long peekLong () {
        int i = top;
        if (!isLoaded.get(i)) {
            loadLongAt(i-1);
        }
        return Types.intsToLong( slots[i], slots[i-1]);
    }

    public long peekLong (int offset) {
        int i = top - offset;
        if (!isLoaded.get(i)) {
            loadLongAt(i-1);
        }
        return Types.intsToLong( slots[i], slots[i-1]);
    }

    public void push (int v){
        top++;
        slots[top] = v;
        isRef.clear(top);
        isLoaded.set(top);

        //if (attrs != null){ // done on pop
        //  attrs[top] = null;
        //}
    }

    public void push (int v, boolean ref) {
        top++;
        slots[top] = v;
        isRef.set(top, ref);
        isLoaded.set(top);

        //if (attrs != null){ // done on pop
        //  attrs[top] = null;
        //}

        if (ref && (v != MJIEnv.NULL)) {
            VM.getVM().getSystemState().activateGC();
        }
    }

    public void pushLocal (int index) {
        top++;
        slots[top] = slots[index];
        isRef.set(top, isRef.get(index));
        isLoaded.set(top);

        if (attrs != null){
            attrs[top] = attrs[index];
        }
    }

    public void pushLongLocal (int index) {
        int t = top;

        if (!isLoaded.get(index)) {
            loadLongAt(index);
        }

        slots[++t] = slots[index];
        //longSlots[++t] = longSlots[index];
        isRef.clear(t);
        isLoaded.set(t);
        slots[++t] = slots[index+1];
        //longSlots[++t] = longSlots[index+1];
        isRef.clear(t);
        isLoaded.set(t);

        if (attrs != null) {
            attrs[t-1] = attrs[index];
            attrs[t] = null;
        }

        top = t;
    }



    public void pushRef (int ref){
        top++;
        slots[top] = ref;
        isRef.set(top);
        isLoaded.set(top);
        //if (attrs != null){ // done on pop
        //  attrs[top] = null;
        //}

        if (ref != MJIEnv.NULL) {
            VM.getVM().getSystemState().activateGC();
        }
    }

    public void setLocalReferenceVariable (int index, int ref){
        if (slots[index] != MJIEnv.NULL){
            VM.getVM().getSystemState().activateGC();
        }

        slots[index] = ref;
        isRef.set(index);
        isLoaded.set(index);
    }

    public void setLocalVariable (int index, int v){
        // Hmm, should we treat this an error?
        if (isRef.get(index) && slots[index] != MJIEnv.NULL){
            VM.getVM().getSystemState().activateGC();      
        }

        slots[index] = v;
        isRef.clear(index);
        isLoaded.set(index);
    }

    // <2do> replace with non-ref version
    public void setLocalVariable (int index, int v, boolean ref) {
        // <2do> activateGc should be replaced by local refChanged
        boolean activateGc = ref || (isRef.get(index) && (slots[index] != MJIEnv.NULL));

        slots[index] = v;
        isRef.set(index,ref);
        isLoaded.set(index, ref);

        if (activateGc) {
            VM.getVM().getSystemState().activateGC();
        }
    }

    public void setLongLocalVariable(int index, long v) {
        // WATCH OUT: apparently, slots can change type, so we have to
        // reset the reference flag (happened in JavaSeq)

        slots[index] = Types.hiLong(v);
        isRef.clear(index);
        isLoaded.set(index);

        index++;
        slots[index] = Types.loLong(v);
        isRef.clear(index);
        isLoaded.set(index);
    }

    public void setOperand (int offset, int v, boolean isRefValue){
        int i = top-offset;
        slots[i] = v;
        isRef.set(i, isRefValue);
        isLoaded.set(i);
    }

    @Override
    public void swap() {
        int t = top - 1;

        int v = slots[top];
        long lv = longSlots[top];
        boolean isTopRef = isRef.get(top);
        boolean isTopLoaded = isLoaded.get(top);

        slots[top] = slots[t];
        longSlots[top] = longSlots[t];
        isRef.set(top, isRef.get(t));
        isLoaded.set(top, isLoaded.get(t));

        slots[t] = v;
        longSlots[t] = lv;
        isRef.set(t, isTopRef);
        isLoaded.set(t, isTopLoaded);

        if (attrs != null) {
            Object a = attrs[top];
            attrs[top] = attrs[t];
            attrs[t] = a;
        }
    }

    public void storeOperand (int index) {
        slots[index] = slots[top];
        isRef.set( index, isRef.get(top));

        if (attrs != null){
            attrs[index] = attrs[top];
            attrs[top] = null;
        }

        top--;
    }

    public void storeLongOperand (int index){
        int t = top-1;
        int i = index;

        slots[i] = slots[t];
        isRef.clear(i);
        isLoaded.set(i);

        slots[++i] = slots[t+1];
        isRef.clear(i);
        isLoaded.set(i);

        if (attrs != null){
            attrs[index] = attrs[t]; // its in the lower word
            attrs[i] = null;

            attrs[t] = null;
            attrs[t+1] = null;
        }

        top -=2;
    }

    public void setCallArguments (ThreadInfo ti){
        throw new RuntimeException("We set this by ourself");
    }

    public void setJavaFrame(ThreadInfo ti, HeapAdapter ha, StackFrameDesc javaFrame) {
        if (!ha.isLoadedFromHostVM(getClassInfo())) {
            throw new UninterpretableException("JPF has its own implementation of " + mi + " in " + getClassInfo());
        }

        int nof_locals = mi.getMaxLocals();
        int nof_stack = mi.getMaxStack();

        if ((nof_locals + nof_stack) != slots.length) {
            throw new RuntimeException("Sanity check failed!");
        }

        if (slots.length < javaFrame.longSlots.length) {
            throw new InternalErrorException("Broken Java stack");
        }

        if (debug) {
            System.out.format("%s nof_locals=%d, nof_stack=%d, nof_slots=%d, nof_longSlots=%d\n",
                    mi,
                    nof_locals,
                    nof_stack,
                    slots.length,
                    javaFrame.longSlots.length);
        }

        longSlots = new long[slots.length]; 
        System.arraycopy(javaFrame.longSlots, 0, longSlots, 0, javaFrame.longSlots.length);


        int i=0;

        for (; i< nof_locals; i++) {
            long v = javaFrame.longSlots[i];
            Object r = javaFrame.objectSlots[i];

            if (r == null) {
                setLocalDummy(i, (int)v);
            } else {
                if (v != 0) {
                    throw new RuntimeException("Sanity check failed");
                }
                setLocalReferenceVariable(i, ha.getJPFObject(ti, r));
            }
        }

        //int final_length = slots.length < javaFrame.longSlots.length ? slots.length : javaFrame.longSlots.length ; 

        int final_length = javaFrame.longSlots.length;

        for (; i< final_length; i++) {
            long v = javaFrame.longSlots[i];
            Object r = javaFrame.objectSlots[i];

            if (r == null) {
                pushDummy((int)v);
            } else {
                if (v != 0) {
                    throw new RuntimeException("Sanity check failed");
                }
                //System.out.println("Push local reference at " + i + ", r=" + ha.getJPFObject(ti, r) + ", c=" + r.getClass().getName());
                pushRef(ha.getJPFObject(ti, r));
            }
        }

        if (!mi.isStatic()) {
            int ref = this.getLocalVariable(0);
            if (ti.getElementInfo(ref) == null) {
                if (ref != 0) {
                    throw new InternalErrorException("cannot set this. ref is " + ref + " but cannot get the corresponding element info");
                }
                this.setExecutable(false);
            }
            this.setThis(ref);
        }
    }

    void pushDummy(int v) {
        top++;
        slots[top] = v; //0xDEADBEEF;
    }

    void setLocalDummy(int index, int v) {
        slots[index] = v; //0xDEADBEEF;
    }

    protected void printContentsOn(PrintWriter pw) {
        super.printContentsOn(pw);
        pw.print("longSlots[");
        for (int i = 0; i < longSlots.length; i++) {
            if (i == stackBase) {
                pw.print("||");
            } else if (i == top) {
                pw.print(']');
            } else {
                if (i != 0) {
                    pw.print(',');
                }
            }

            if (isRef.get(i)){
                pw.print('@');
            } else {
                pw.print(longSlots[i]);
            }
        }

    }

    @Override
    public long[] getLongSlots() {
        return longSlots;
    }

    private boolean isExecutable;

    @Override
    public boolean isExecutable() {
        return isExecutable;
    }

    @Override
    public void setExecutable(boolean value) {
        isExecutable = value;
    }
}
