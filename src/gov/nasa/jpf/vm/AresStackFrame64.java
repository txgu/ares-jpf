package gov.nasa.jpf.vm;

import gov.nasa.jpf.jvm.JVMStackFrame;
import gov.nasa.jpf.util.FixedBitSet;
import gov.nasa.jpf.util.HashData;
import gov.nasa.jpf.util.Misc;
import gov.nasa.jpf.util.OATHash;
import gov.nasa.jpf.util.ObjectList;
import gov.nasa.jpf.util.PrintUtils;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;

public class AresStackFrame64 extends JVMStackFrame implements AresHostStackFrame64{

    protected long[] slots;

    public AresStackFrame64(MethodInfo callee) {
        super(callee);

        slots = new long[super.slots.length];
        super.slots = new int[0];
    }

    private int l2i(int i) {
        return (int) slots[i];
    }

    /**
     * return reference of called object in the context of the caller
     * (i.e. we are in the caller frame)
     */
    public int getCalleeThis (int size) {
        // top is the topmost index
        int i = size-1;
        if (top < i) {
            return MJIEnv.NULL;
        }

        return l2i(top-i);
    }

    public Object getLocalValueObject (LocalVarInfo lv) {
        if (lv != null) { // might not have been compiled with debug info
            String sig = lv.getSignature();
            int slotIdx = lv.getSlotIndex();
            long v = slots[slotIdx];

            switch (sig.charAt(0)) {
            case 'Z':
                return Boolean.valueOf(v != 0);
            case 'B':
                return new Byte((byte) v);
            case 'C':
                return new Character((char) v);
            case 'S':
                return new Short((short) v);
            case 'I':
                return new Integer((int) v);
            case 'J':
                return new Long(v); // Java is big endian, Types expects low,high
            case 'F':
                return new Float(Float.intBitsToFloat((int)v));
            case 'D':
                return new Double(Double.longBitsToDouble(v));
            default:  // reference
                if (v >= 0) {
                    return VM.getVM().getHeap().get((int)v);
                }
            }
        }

        return null;
    }

    /**
     * does any of the 'nTopSlots' hold callerSlots reference value of 'objRef'
     * 'nTopSlots' is usually obtained from MethodInfo.getNumberOfCallerStackSlots()
     */
    public boolean includesReferenceOperand (int nTopSlots, int objRef) {

        for (int i=0, j=top-nTopSlots+1; i<nTopSlots && j>=0; i++, j++) {
            if (isRef.get(j) && (slots[j] == objRef)){
                return true;
            }
        }

        return false;
    }

    /**
     * does any of the operand slots hold callerSlots reference value of 'objRef'
     */
    public boolean includesReferenceOperand (int objRef){
        for (int i=stackBase; i<=top; i++) {
            if (isRef.get(i) && (slots[i] == objRef)){
                return true;
            }
        }

        return false;
    }

    /**
     * generic visitor for reference arguments
     */
    public void processRefArguments (MethodInfo miCallee, ReferenceProcessor visitor){
        int nArgSlots = miCallee.getArgumentsSize();

        for (int i=top-1; i>=top-nArgSlots; i--){
            if (isRef.get(i)){
                visitor.processReference((int)slots[i]);
            }
        }
    }

    public int getSlot(int idx){
        return l2i(idx);
    }

    public boolean isReferenceSlot(int idx){
        return isRef.get(idx);
    }


    public void setOperand (int offset, int v, boolean isRefValue){
        int i = top-offset;
        slots[i] = v;
        isRef.set(i, isRefValue);
    }


    // -- end attrs --

    public void setLocalReferenceVariable (int index, int ref){
        if (slots[index] != MJIEnv.NULL){
            VM.getVM().getSystemState().activateGC();
        }

        slots[index] = ref;
        isRef.set(index);
    }

    public void setLocalVariable (int index, int v){
        // Hmm, should we treat this an error?
        if (isRef.get(index) && slots[index] != MJIEnv.NULL){
            VM.getVM().getSystemState().activateGC();      
        }

        slots[index] = v;
        isRef.clear(index);
    }

    // <2do> replace with non-ref version
    public void setLocalVariable (int index, int v, boolean ref) {
        // <2do> activateGc should be replaced by local refChanged
        boolean activateGc = ref || (isRef.get(index) && (slots[index] != MJIEnv.NULL));

        slots[index] = v;
        isRef.set(index,ref);

        if (activateGc) {
            VM.getVM().getSystemState().activateGC();
        }
    }

    public int getLocalVariable (int i) {
        return l2i(i);
    }


    //--- direct slot access - provided for machine-independent clients

    public int[] getSlots () {
        //return slots; // we should probably clone
        throw new RuntimeException();
    }

    public long[] getLongSlots() {
        return slots;
    }

    public void visitReferenceSlots (ReferenceProcessor visitor){
        for (int i=isRef.nextSetBit(0); i>=0 && i<=top; i=isRef.nextSetBit(i+1)){
            visitor.processReference((int)slots[i]);
        }
    }

    public void setLongLocalVariable (int index, long v) {
        // WATCH OUT: apparently, slots can change type, so we have to
        // reset the reference flag (happened in JavaSeq)


        isRef.clear(index);

        index++;
        slots[index] = v;
        isRef.clear(index);
    }

    public long getLongLocalVariable (int idx) {
        return slots[idx+1];
    }

    public double getDoubleLocalVariable (int idx) {
        return Double.longBitsToDouble(slots[idx + 1]);
    }

    public float getFloatLocalVariable (int idx) {
        int bits = l2i(idx);
        return Float.intBitsToFloat(bits);
    }


    // this is callerSlots deep copy
    public AresStackFrame64 clone () {
        AresStackFrame64 sf = (AresStackFrame64) super.clone();

        sf.defreeze();

        sf.slots = slots.clone();
        sf.isRef = isRef.clone();

        if (attrs != null){
            sf.attrs = attrs.clone();
        }

        // frameAttr is not cloned to allow search global use 

        return sf;
    }

    public void dup () {
        // .. A     =>
        // .. A A
        //    ^

        int t= top;

        int td=t+1;
        slots[td] = slots[t];
        isRef.set(td, isRef.get(t));

        if (attrs != null){
            attrs[td] = attrs[t];
        }

        top = td;
    }

    public void dup2 () {
        // .. A B        =>
        // .. A B A B
        //      ^

        int ts, td;
        int t=top;

        // duplicate A
        td = t+1; ts = t-1;
        slots[td] = slots[ts];
        isRef.set(td, isRef.get(ts));
        if (attrs != null){
            attrs[td] = attrs[ts];
        }

        // duplicate B
        td++; ts=t;
        slots[td] = slots[ts];
        isRef.set(td, isRef.get(ts));
        if (attrs != null){
            attrs[td] = attrs[ts];
        }

        top = td;
    }

    public void dup2_x1 () {
        // .. A B C       =>
        // .. B C A B C
        //        ^

        long b, c;
        boolean bRef, cRef;
        Object bAnn = null, cAnn = null;
        int ts, td;
        int t = top;

        // duplicate C
        ts=t; td = t+2;                              // ts=top, td=top+2
        slots[td] = c = slots[ts];
        cRef = isRef.get(ts);
        isRef.set(td,cRef);
        if (attrs != null){
            attrs[td] = cAnn = attrs[ts];
        }

        // duplicate B
        ts--; td--;                                  // ts=top-1, td=top+1
        slots[td] = b = slots[ts];
        bRef = isRef.get(ts);
        isRef.set(td, bRef);
        if (attrs != null){
            attrs[td] = bAnn = attrs[ts];
        }

        // shuffle A
        ts=t-2; td=t;                                // ts=top-2, td=top
        slots[td] = slots[ts];
        isRef.set(td, isRef.get(ts));
        if (attrs != null){
            attrs[td] = attrs[ts];
        }

        // shuffle B
        td = ts;                                     // td=top-2
        slots[td] = b;
        isRef.set(td, bRef);
        if (attrs != null){
            attrs[td] = bAnn;
        }

        // shuffle C
        td++;                                        // td=top-1
        slots[td] = c;
        isRef.set(td, cRef);
        if (attrs != null){
            attrs[td] = cAnn;
        }

        top += 2;
    }

    public void dup2_x2 () {
        // .. A B C D       =>
        // .. C D A B C D
        //          ^

        long c, d;
        boolean cRef, dRef;
        Object cAnn = null, dAnn = null;
        int ts, td;
        int t = top;

        // duplicate C
        ts = t-1; td = t+1;                          // ts=top-1, td=top+1
        slots[td] = c = slots[ts];
        cRef = isRef.get(ts);
        isRef.set(td, cRef);
        if (attrs != null){
            attrs[td] = cAnn = attrs[ts];
        }

        // duplicate D
        ts=t; td++;                                  // ts=top, td=top+2
        slots[td] = d = slots[ts];
        dRef = isRef.get(ts);
        isRef.set(td, dRef);
        if (attrs != null){
            attrs[td] = dAnn = attrs[ts];
        }

        // shuffle A
        ts = t-3; td = t-1;                          // ts=top-3, td=top-1
        slots[td] = slots[ts];
        isRef.set( td, isRef.get(ts));
        if (attrs != null){
            attrs[td] = attrs[ts];
        }

        // shuffle B
        ts++; td = t;                                // ts = top-2
        slots[td] = slots[ts];
        isRef.set( td, isRef.get(ts));
        if (attrs != null){
            attrs[td] = attrs[ts];
        }

        // shuffle D
        td = ts;                                     // td = top-2
        slots[td] = d;
        isRef.set( td, dRef);
        if (attrs != null){
            attrs[td] = dAnn;
        }

        // shuffle C
        td--;                                        // td = top-3
        slots[td] = c;
        isRef.set(td, cRef);
        if (attrs != null){
            attrs[td] = cAnn;
        }

        top += 2;
    }

    public void dup_x1 () {
        // .. A B     =>
        // .. B A B
        //      ^

        long b;
        boolean bRef;
        Object bAnn = null;
        int ts, td;
        int t = top;

        // duplicate B
        ts = t; td = t+1;
        slots[td] = b = slots[ts];
        bRef = isRef.get(ts);
        isRef.set(td, bRef);
        if (attrs != null){
            attrs[td] = bAnn = attrs[ts];
        }

        // shuffle A
        ts--; td = t;       // ts=top-1, td = top
        slots[td] = slots[ts];
        isRef.set( td, isRef.get(ts));
        if (attrs != null){
            attrs[td] = attrs[ts];
        }

        // shuffle B
        td = ts;            // td=top-1
        slots[td] = b;
        isRef.set( td, bRef);
        if (attrs != null){
            attrs[td] = bAnn;
        }

        top++;
    }

    public void dup_x2 () {
        // .. A B C     =>
        // .. C A B C
        //        ^

        long c;
        boolean cRef;
        Object cAnn = null;
        int ts, td;
        int t = top;

        // duplicate C
        ts = t; td = t+1;
        slots[td] = c = slots[ts];
        cRef = isRef.get(ts);
        isRef.set( td, cRef);
        if (attrs != null){
            attrs[td] = cAnn = attrs[ts];
        }

        // shuffle B
        td = ts; ts--;               // td=top, ts=top-1
        slots[td] = slots[ts];
        isRef.set( td, isRef.get(ts));
        if (attrs != null){
            attrs[td] = attrs[ts];
        }

        // shuffle A
        td=ts; ts--;                 // td=top-1, ts=top-2
        slots[td] = slots[ts];
        isRef.set( td, isRef.get(ts));
        if (attrs != null){
            attrs[td] = attrs[ts];
        }

        // shuffle C
        td = ts;                     // td = top-2
        slots[td] = c;
        isRef.set(td, cRef);
        if (attrs != null){
            attrs[td] = cAnn;
        }

        top++;
    }


    // <2do> pcm - I assume this compares snapshots, not types. Otherwise it
    // would be pointless to equals stack/local values
    public boolean equals (Object o) {
        if (o instanceof AresStackFrame64){
            StackFrame other = (StackFrame)o;

            if (prev != other.prev) {
                return false;
            }
            if (pc != other.pc) {
                return false;
            }
            if (mi != other.mi) {
                return false;
            }
            if (top != other.top){
                return false;
            }

            int[] otherSlots = other.slots;
            FixedBitSet otherIsRef = other.isRef;
            for (int i=0; i<=top; i++){
                if ( slots[i] != otherSlots[i]){
                    return false;
                }
                if ( isRef.get(i) != otherIsRef.get(i)){
                    return false;
                }
            }

            if (!Misc.compare(top,attrs,other.attrs)){
                return false;
            }

            if (!ObjectList.equals(frameAttr, other.frameAttr)){
                return false;
            }

            return true;
        }

        return false;
    }

    public int mixinExecutionStateHash (int h) {
        h = OATHash.hashMixin( h, mi.getGlobalId());

        if (pc != null){
            h = OATHash.hashMixin(h, pc.getInstructionIndex());
            // we don't need the bytecode since there can only be one insn with this index in this method
        }

        // TODO
        for (int i=0; i<top; i++) {
            h = OATHash.hashMixin(h, slots[i]);
        }

        return h;
    }

    protected void hash (HashData hd) {
        if (prev != null){
            hd.add(prev.objectHashCode());
        }
        hd.add(mi.getGlobalId());

        if (pc != null){
            hd.add(pc.getInstructionIndex());
        }

        for (int i=0; i<=top; i++){
            hd.add(slots[i]);
        }

        int ls = isRef.longSize();
        for (int i=0; i<ls; i++){
            hd.add(isRef.getLong(i));
        }

        // it's debatable if we add the attributes to the state, but whatever it
        // is, it should be kept consistent with the Fields.hash()
        if (attrs != null){
            for (int i=0; i<=top; i++){
                ObjectList.hash( attrs[i], hd);
            }
        }

        if (frameAttr != null){
            ObjectList.hash(frameAttr, hd);
        }
    }

    // computes an hash code for the hash table
    // the default hash code is different for each object
    // we need to redifine it to make the hash table work
    public int hashCode () {
        HashData hd = new HashData();
        hash(hd);
        return hd.getValue();
    }

    /**
     * mark all objects reachable from local or operand stack positions containing
     * references. Done during phase1 marking of threads (the stack is one of the
     * Thread gc roots)
     */
    public void markThreadRoots (Heap heap, int tid) {

        /**
	    for (int i = isRef.nextSetBit(0); i>=0 && i<=top; i = isRef.nextSetBit(i + 1)) {
	      int objref = slots[i];
	      if (objref != MJIEnv.NULL) {
	        heap.markThreadRoot(objref, tid);
	      }
	    }
         **/
        for (int i = 0; i <= top; i++) {
            if (isRef.get(i)) {
                int objref = l2i(i);
                if (objref != MJIEnv.NULL) {
                    heap.markThreadRoot(objref, tid);
                }
            }
        }
    }

    //--- debugging methods

    public void printOperands (PrintStream pw){
        pw.print("operands = [");
        for (int i=stackBase; i<=top; i++){
            if (i>0){
                pw.print(',');
            }
            if (isOperandRef(i)){
                pw.print('^');
            }
            pw.print(slots[i]);
            Object a = getOperandAttr(top-i);
            if (a != null){
                pw.print(" {");
                pw.print(a);
                pw.print('}');
            }
        }
        pw.println(']');
    }

    /**
     * this includes locals and pc
     */
    public void printStackContent () {
        PrintStream pw = System.out;

        pw.print( "\tat ");
        pw.print( mi.getFullName());

        if (pc != null) {
            pw.println( ":" + pc.getPosition());
        } else {
            pw.println();
        }

        pw.print("\t slots: ");
        for (int i=0; i<=top; i++){
            if (i == stackBase){
                pw.println("\t      ----------- operand stack");
            }

            pw.print( "\t    [");
            pw.print(i);
            pw.print("] ");
            if (isRef.get(i)) {
                pw.print( "@");
            }
            pw.print( slots[i]);

            if (attrs != null){
                pw.print("  attr=");
                pw.print(attrs[i]);
            }

            pw.println();
        }
    }

    public void printStackTrace () {
        System.out.println( getStackTraceInfo());
    }

    public void swap () {
        int t = top-1;

        long v = slots[top];
        boolean isTopRef = isRef.get(top);

        slots[top] = slots[t];
        isRef.set( top, isRef.get(t));

        slots[t] = v;
        isRef.set( t, isTopRef);

        if (attrs != null){
            Object a = attrs[top];
            attrs[top] = attrs[t];
            attrs[t] = a;
        }
    }


    // <2do> there are way too many different print/debug methods here
    public void printSlots (PrintStream ps){
        for (int i = 0; i <= top; i++) {
            if (i == stackBase){
                ps.print("||");
            } else {
                if (i != 0) {
                    ps.print(',');
                }
            }

            if (isRef.get(i)){
                PrintUtils.printReference(ps, (int)slots[i]);
            } else {
                ps.print(slots[i]);
            }
        }    
    }

    public int getDepth(){
        int depth = 0;

        for (StackFrame frame = prev; frame != null; frame = frame.prev){
            depth++;
        }

        return depth;
    }

    protected int objectHashCode() {
        return super.hashCode();
    }

    public String toString () {
        StringWriter sw = new StringWriter(128);
        PrintWriter pw = new PrintWriter(sw);

        pw.print(getClass().getSimpleName() + '{');
        //pw.print(Integer.toHexString(objectHashCode()));
        printContentsOn(pw);
        pw.print('}');

        return sw.toString();
    }

    public float peekFloat() {
        return Float.intBitsToFloat(l2i(top));
    }

    public float peekFloat (int offset){
        return Float.intBitsToFloat(l2i(top-offset));
    }

    public double peekDouble() {
        int i = top;
        return Double.longBitsToDouble(slots[i]);
    }

    public double peekDouble (int offset){
        int i = top-offset;
        return Double.longBitsToDouble(slots[i]);
    }

    public long peekLong () {
        int i = top;
        return slots[i];
    }

    public long peekLong (int offset) {
        int i = top - offset;
        return slots[i];
    }

    public void pushLong (long v) {
        top++;
        isRef.clear(top);
        top++;
        slots[top] = v;
        isRef.clear(top);
    }

    public void pushDouble (double v) {
        long l = Double.doubleToLongBits(v);
        pushLong(l);
    }

    public void pushFloat (float v) {
        push( Float.floatToIntBits(v));
    }

    public double popDouble () {
        int i = top;

        long l = slots[i--];
        i--;

        if (attrs != null){
            i = top;
            attrs[i--] = null; // not really required
            attrs[i--] = null; // that's where the attribute should be
        }

        top = i;
        return Double.longBitsToDouble(l);
    }

    public long popLong () {
        int i = top;

        long l = slots[i--];
        i--;

        if (attrs != null){
            i = top;
            attrs[i--] = null; // not really required
            attrs[i--] = null; // that's where the attribute should be
        }

        top = i;
        return l;
    }

    public int peek () {
        return l2i(top);
    }

    public int peek (int offset) {
        return l2i(top-offset);
    }

    public void pop (int n) {
        //assert (top >= stackBase) : "stack empty";

        int t = top - n;

        // <2do> get rid of this !
        for (int i=top; i>t; i--) {
            if (isRef.get(i) && (slots[i] != MJIEnv.NULL)) {
                VM.getVM().getSystemState().activateGC();
                break;
            }
        }

        if (attrs != null){  // just to avoid memory leaks
            for (int i=top; i>t; i--){
                attrs[i] = null;
            }
        }

        top = t;
    }

    public float popFloat() {    
        int v = l2i(top);

        if (attrs != null){ // just to avoid memory leaks
            attrs[top] = null;
        }

        top--;

        return Float.intBitsToFloat(v);
    }

    public int pop () {
        //assert (top >= stackBase) : "stack empty";

        long v = slots[top];

        // <2do> get rid of this
        if (isRef.get(top)) {
            if (v != MJIEnv.NULL) {
                VM.getVM().getSystemState().activateGC();
            }
        }

        if (attrs != null){ // just to avoid memory leaks
            attrs[top] = null;
        }

        top--;

        // note that we don't reset the operands or oRefs values, so that
        // we can still access them after the insn doing the pop got executed
        // (e.g. useful for listeners)

        return (int)(v);
    }

    public void pushLocal (int index) {
        top++;
        slots[top] = slots[index];
        isRef.set(top, isRef.get(index));

        if (attrs != null){
            attrs[top] = attrs[index];
        }
    }

    public void pushLongLocal (int index) {
        int t = top;

        slots[++t] = slots[index];
        isRef.clear(t);
        slots[++t] = slots[index+1];
        isRef.clear(t);

        if (attrs != null){
            attrs[t-1] = attrs[index];
            attrs[t] = null;
        }

        top = t;
    }

    public void storeOperand (int index){
        slots[index] = slots[top];
        isRef.set( index, isRef.get(top));

        if (attrs != null){
            attrs[index] = attrs[top];
            attrs[top] = null;
        }

        top--;
    }

    public void storeLongOperand (int index){
        int t = top-1;
        int i = index;

        slots[i] = slots[t];
        isRef.clear(i);

        slots[++i] = slots[t+1];
        isRef.clear(i);

        if (attrs != null){
            attrs[index] = attrs[t]; // its in the lower word
            attrs[i] = null;

            attrs[t] = null;
            attrs[t+1] = null;
        }

        top -=2;
    }

    public void push (int v){
        top++;
        slots[top] = v;
        isRef.clear(top);

        //if (attrs != null){ // done on pop
        //  attrs[top] = null;
        //}
    }

    public void pushRef (int ref){
        top++;
        slots[top] = ref;
        isRef.set(top);

        //if (attrs != null){ // done on pop
        //  attrs[top] = null;
        //}

        if (ref != MJIEnv.NULL) {
            VM.getVM().getSystemState().activateGC();
        }
    }

    public void push (int v, boolean ref) {
        top++;
        slots[top] = v;
        isRef.set(top, ref);

        //if (attrs != null){ // done on pop
        //  attrs[top] = null;
        //}

        if (ref && (v != MJIEnv.NULL)) {
            VM.getVM().getSystemState().activateGC();
        }
    }

    protected void setCallArguments32 (StackFrame caller) {
        MethodInfo miCallee = mi;
        int nArgSlots = miCallee.getArgumentsSize();
        long[] calleeSlots = slots;
        FixedBitSet calleeRefs = isRef;

        int[] callerSlots = caller.getSlots();
        FixedBitSet callerRefs = caller.getReferenceMap();

        for (int i = 0, j = caller.getTopPos() - nArgSlots + 1; i < nArgSlots; i++, j++) {
            calleeSlots[i] = callerSlots[j];
            if (callerRefs.get(j)) {
                calleeRefs.set(i);
            }
            Object a = caller.getSlotAttr(j);
            if (a != null) {
                setSlotAttr(i, a);
            }
        }

        if (!miCallee.isStatic()) {
            thisRef = (int)calleeSlots[0];
        }
    }

    protected void setCallArguments64 (AresStackFrame64 caller) {
        MethodInfo miCallee = mi;
        int nArgSlots = miCallee.getArgumentsSize();
        long[] calleeSlots = slots;
        FixedBitSet calleeRefs = isRef;

        long[] callerSlots = caller.getLongSlots();
        FixedBitSet callerRefs = caller.getReferenceMap();

        for (int i = 0, j = caller.getTopPos() - nArgSlots + 1; i < nArgSlots; i++, j++) {
            calleeSlots[i] = callerSlots[j];
            if (callerRefs.get(j)) {
                calleeRefs.set(i);
            }
            Object a = caller.getSlotAttr(j);
            if (a != null) {
                setSlotAttr(i, a);
            }
        }

        if (!miCallee.isStatic()) {
            thisRef = (int)calleeSlots[0];
        }
    }

    public void setCallArguments (ThreadInfo ti){
        StackFrame caller = ti.getTopFrame();
        MethodInfo miCallee = mi;
        int nArgSlots = miCallee.getArgumentsSize();

        if (nArgSlots > 0) {
            if (caller instanceof AresStackFrame64) {
                setCallArguments64((AresStackFrame64) caller);
            } else {
                setCallArguments32(caller);
            }
        }
    }

    private boolean isExecutable;

    @Override
    public boolean isExecutable() {
        return isExecutable;
    }

    @Override
    public void setExecutable(boolean value) {
        isExecutable = value;
    }
}
