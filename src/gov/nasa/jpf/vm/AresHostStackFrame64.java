package gov.nasa.jpf.vm;

public interface AresHostStackFrame64 extends AresHostStackFrame {
    long[] getLongSlots();
}
