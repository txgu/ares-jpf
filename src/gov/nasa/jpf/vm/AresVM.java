package gov.nasa.jpf.vm;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.JPFConfigException;
import gov.nasa.jpf.JPFException;
import gov.nasa.jpf.JPFListenerException;
import gov.nasa.jpf.Ares;
import gov.nasa.jpf.jvm.RecoveryHelper;

import java.util.Collections;
import java.util.List;

import org.javelus.ares.EarlyStopException;
import org.javelus.ares.CascadedErrorException;
import org.javelus.ares.FailureDesc;
import org.javelus.ares.HeapAdapter;
import org.javelus.ares.StackBuilder;
import org.javelus.ares.TimeoutException;
import org.javelus.ares.UninterpretableException;
import org.javelus.ares.recovery.RecoveryResult;
import org.javelus.ares.recovery.SimpleStrategy;
import org.javelus.ares.recovery.RecoveryAction;
import org.javelus.ares.recovery.RecoveryStrategy;

public class AresVM extends SingleProcessVM {

    public static final boolean debug = Ares.debugAresVM;

    protected HeapAdapter ha;
    protected StackBuilder stackBuilder;

    protected ThreadInfo tiMain;

    protected FailureDesc currentFailure;

    protected RecoveryStrategy recoveryStrategy;

    public AresVM (JPF jpf, Config conf) {
        super(jpf, conf);
    }

    protected AresVM() {}

    protected ApplicationContext createApplicationContext() {
        SystemClassLoaderInfo sysCli = createSystemClassLoaderInfo(0);
        return new ApplicationContext( 0, null, null, EMPTY_ARGS, null, sysCli);
    }

    public boolean forward () {

        // the reason we split up CG initialization and transition execution
        // is that program state storage is not required if the CG initialization
        // does not produce a new choice since we have to backtrack in that case
        // anyways. This can be caused by complete enumeration of CGs and/or by
        // CG listener intervention (i.e. not just after backtracking). For a large
        // number of matched or end states and ignored transitions this can be a
        // huge saving.
        // The downside is that CG notifications are NOT allowed anymore to change the
        // KernelState (modify fields or thread states) since those changes would
        // happen before storing the KernelState, and hence would make backtracking
        // inconsistent. This is advisable anyways since all program state changes
        // should take place during transitions, but the real snag is that this
        // cannot be easily enforced.

        // actually, it hasn't occurred yet, but will
        transitionOccurred = ss.initializeNextTransition(this);

        if (transitionOccurred){
            if (CHECK_CONSISTENCY) {
                checkConsistency(true); // don't push an inconsistent state
            }

            backtracker.pushKernelState();

            // cache this before we enter (and increment) the next insn(s)
            lastTrailInfo = path.getLast();


            try {
                ss.executeNextTransition(vm);
            } catch (UncaughtException e) {
                // we don't pass this up since it means there were insns executed and we are
                // in a consistent state
            } // every other exception goes upwards

            backtracker.pushSystemState();
            updatePath();

            if (!isIgnoredState()) {
                // if this is ignored we are going to backtrack anyways
                // matching states out of ignored transitions is also not a good idea
                // because this transition is usually incomplete

                if (runGc && !hasPendingException()) {
                    if(ss.gcIfNeeded()) {
                        processFinalizers();
                    }
                }

                if (stateSet != null) {
                    newStateId = stateSet.size();
                    int id = stateSet.addCurrent();
                    ss.setId(id);

                } else { // this is 'state-less' model checking, i.e. we don't match states
                    ss.setId(++newStateId); // but we still should have states numbered in case listeners use the id
                }
            }

            return true;

        } else {
            return false;  // no transition occurred
        }
    }

    protected ThreadInfo initializeMainThread (ApplicationContext appCtx, int tid) {
        SystemClassLoaderInfo sysCl = appCtx.sysCl;

        ThreadInfo tiMain = createMainThreadInfo(tid, appCtx);

        List<ClassInfo> startupClasses = getStartupSystemClassInfos(sysCl, tiMain);

        if (!checkSystemClassCompatibility( sysCl)){
            throw new JPFConfigException("non-JPF system classes, check classpath");
        }

        // create essential objects (we can't call ctors yet)
        createSystemClassLoaderObject(sysCl, tiMain);
        for (ClassInfo ci : startupClasses) {
            ci.createAndLinkStartupClassObject(tiMain);
        }
        tiMain.createMainThreadObject(sysCl);
        registerThread(tiMain);

        // note that StackFrames have to be pushed in reverse order
        Collections.reverse(startupClasses);
        pushClinits(startupClasses, tiMain);

        registerThreadListCleanup(sysCl.getThreadClassInfo());

        return tiMain;
    }

    @Override
    public boolean initialize() {
        try {
            // this has to happen before we load the startup classes during initializeMainThread
            scheduler.initialize(this, appCtx);

            // MJIEnv will use ha
            initializeAres();

            tiMain = initializeMainThread(appCtx, 0);
            initializeFinalizerThread(appCtx, 1);

            if (tiMain == null) {
                return false; // bail out
            }

            initSystemState(tiMain);

            initialized = true;
            notifyVMInitialized();
            return true;
        } catch (JPFConfigException cfe){
            log.severe(cfe.getMessage());
            return false;
        } catch (ClassInfoException cie){
            log.severe(cie.getMessage());
            return false;
        }
        // all other exceptions are JPF errors that should cause stack traces
    }

    protected void initializeAres() {
        ha = new HeapAdapter(this);
        stackBuilder = new StackBuilder(ha);
    }

    public HeapAdapter getHeapAdapter() {
        return ha;
    }

    protected FailureDesc buildFailureDesc(Object[] data) {
        return FailureDesc.buildFailureDesc(data);
    }

    public void buildFailureThread(Object[] data) {
        currentFailure = buildFailureDesc(data);
        stackBuilder.pushStackFrames(tiMain,
                currentFailure.getCauseEception(),
                currentFailure.getStackFrames());
    }

    public void buildRecoveryStrategy() {
        recoveryStrategy = new SimpleStrategy();
        recoveryStrategy.buildRecoveryActions(getRecoveryThread(), getCauseExceptionRef());
    }

    public ThreadInfo getRecoveryThread() {
        return tiMain;
    }

    public int getCauseExceptionRef() {
        return ha.getJPFObject(getRecoveryThread(), getJavaCauseException());
    }

    public Object getJavaCauseException() {
        return this.currentFailure.getCauseEception();
    }

    public boolean hasNextRecoveryAction() {
        return recoveryStrategy.hasNext();
    }

    public void printResults() {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        this.recoveryStrategy.printResults();
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    }

    protected RecoveryResult runRecoveryAction(RecoveryAction nextAction) {
        try {
            recoveryStrategy.beginAction(getRecoveryThread(), nextAction);
            ss.executeNextTransition(vm);
            if (Ares.PrintResults) {
                System.out.println("Recovery action " + nextAction + " resulted in no error.");
            }
            return recoveryStrategy.caseNoError(getRecoveryThread(), nextAction);
        } catch (UncaughtException e) {
            if (Ares.PrintResults) {
                // we don't pass this up since it means there were insns executed and we are
                // in a consistent state
                System.out.println("Recovery action " + nextAction + " resulted in " + e);
                if (Ares.PrintStackTrace) {
                    e.printStackTrace();
                }
            }
            if (isUninterpretable(e.xClsName)) {
                return recoveryStrategy.caseUninterpretableError(getRecoveryThread(), nextAction);
            }else {
                return recoveryStrategy.caseCascadedError(getRecoveryThread(), nextAction);
            }
        } catch (UninterpretableException e) {
            if (Ares.PrintResults) {
                System.out.println("Recovery action " + nextAction + " resulted in " + e);
                if (Ares.PrintStackTrace) {
                    e.printStackTrace();
                }
            }
            return recoveryStrategy.caseUninterpretableError(getRecoveryThread(), nextAction);
        } catch (TimeoutException e) {
            if (Ares.PrintResults) {
                System.out.println("Recovery action " + nextAction + " resulted in " + e);
                if (Ares.PrintStackTrace) {
                    e.printStackTrace();
                }
            }
            return recoveryStrategy.caseTimeout(getRecoveryThread(), nextAction);
        } catch (EarlyStopException e) {
            if (Ares.PrintResults) {
                System.out.println("Recovery action " + nextAction + " resulted in " + e);
                if (Ares.PrintStackTrace) {
                    e.printStackTrace();
                }
            }
            return recoveryStrategy.caseEarlyStop(getRecoveryThread(), nextAction);
        } catch (CascadedErrorException e) {
            if (Ares.PrintResults) {
                System.out.println("Recovery action " + nextAction + " resulted in " + e);
                if (Ares.PrintStackTrace) {
                    e.printStackTrace();
                }
            }
            return recoveryStrategy.caseCascadedError(getRecoveryThread(), nextAction);
        } catch (JPFException e) {
            Throwable cause = e.getCause();
            if (Ares.PrintResults) {
                System.out.println("Recovery action " + nextAction + " resulted in " + cause);
                if (Ares.PrintStackTrace) {
                    e.printStackTrace();
                }
            }

            if (cause instanceof UninterpretableException) {
                return recoveryStrategy.caseUninterpretableError(getRecoveryThread(), nextAction);
            } else if (cause instanceof EarlyStopException) {
                return recoveryStrategy.caseEarlyStop(getRecoveryThread(), nextAction);
            } else if (cause instanceof CascadedErrorException) {
                return recoveryStrategy.caseCascadedError(getRecoveryThread(), nextAction);
            } else if (cause instanceof TimeoutException) {
                return recoveryStrategy.caseTimeout(getRecoveryThread(), nextAction);
            } else {
                if (!Ares.PrintResults || Ares.PrintInternalError) {
                    System.out.println("Recovery action " + nextAction + " resulted in " + cause);
                    e.printStackTrace();
                }
                return recoveryStrategy.caseInternalError(getRecoveryThread(), nextAction);
            }
        } catch (RuntimeException e) {
            if (Ares.PrintResults || Ares.PrintInternalError) {
                System.out.println("Recovery action " + nextAction + " resulted in " + e);
                if (Ares.PrintStackTrace || Ares.PrintInternalError) {
                    e.printStackTrace();
                }
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
            }
            return recoveryStrategy.caseInternalError(getRecoveryThread(), nextAction);
        } catch (Throwable e) {
            if (Ares.PrintResults || Ares.PrintInternalError) {
                System.out.println("Recovery action " + nextAction + " resulted in " + e);
                if (Ares.PrintStackTrace || Ares.PrintInternalError) {
                    e.printStackTrace();
                }
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
            }
            recoveryStrategy.caseInternalError(getRecoveryThread(), nextAction);
            throw e;
        } finally {
            recoveryStrategy.endAction(getRecoveryThread(), nextAction);
        }
    }

    public boolean recoverAndForward() {
        transitionOccurred = ss.initializeNextTransition(this);

        if (transitionOccurred) {
            if (CHECK_CONSISTENCY) {
                checkConsistency(true); // don't push an inconsistent state
            }

            backtracker.pushKernelState();
            // cache this before we enter (and increment) the next insn(s)
            lastTrailInfo = path.getLast();

            RecoveryAction nextAction = recoveryStrategy.next();
            boolean applied = false;
            try {
                if (Ares.PrintResults) {
                    System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                    System.out.println("Using recovery action " + nextAction);
                }
                applied = nextAction.recover();
            } catch (Throwable e) {
                if (Ares.PrintResults) {
                    System.out.println("Recovery action " + nextAction + " cannot be applied.");
                    System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                }
            }

            if (applied) {
                RecoveryResult result = runRecoveryAction(nextAction);
                if (Ares.PrintResults) {
                    System.out.println("RecoveryResult is " + result);
                    System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
                }

            }

            backtracker.pushSystemState();
            updatePath();

            if (!isIgnoredState()) {

                if (runGc && !hasPendingException()) {
                    if(ss.gcIfNeeded()) {
                        processFinalizers();
                        ha.gc();
                    }
                }
            }

            return true;
        } else {
            return false;  // no transition occurred
        }
    }

    public void resetRecoveryThread() {
        ThreadInfo ti = getRecoveryThread();
        StackFrame top = ti.getTopFrame();
        for (StackFrame frame = top; frame != null; frame = frame.getPrevious()) {
            //ti.leave();
            ti.popFrame();
        }

        ha.clearInstanceObjects();
    }

    public ThreadInfo getMainThread() {
        return tiMain;
    }

    public void runSystemClassClinits() {
        ChoiceGenerator<?> rootCg = getNextChoiceGenerator();
        if (forward()) {
            rootCg.reset();
            ss.curCg = null;
            if (ss.nextCg != null ) {
                throw new RuntimeException("sanity check");
            }
            setMandatoryNextChoiceGenerator(rootCg, "Cannot reset rootCg");
            return;
        }
        throw new RuntimeException("Fail to run system class inits");
    }

    public RecoveryAction getFinalRecoveryAction() {
        return recoveryStrategy.getBest();
    }

    public Object[] getEncodedFinalRecoveryAction() {
        return recoveryStrategy.getEncodedBest();
    }

    protected void notifyMethodEntered(ThreadInfo ti, MethodInfo mi) {
        try {
            for (int i = 0; i < listeners.length; i++) {
                listeners[i].methodEntered(this, ti, mi);
            }
        } catch (UncaughtException x) {
            throw x;
        } catch (UninterpretableException x) {
            throw x;
        } catch (EarlyStopException x) {
            throw x;
        } catch (CascadedErrorException x) {
            throw x;
        } catch (JPF.ExitException x) {
            throw x;
        } catch (Throwable t) {
            throw new JPFListenerException("exception during methodEntered() notification", t);
        }
    }

    protected void notifyMethodExited(ThreadInfo ti, MethodInfo mi) {
        try {
            for (int i = 0; i < listeners.length; i++) {
                listeners[i].methodExited(this, ti, mi);
            }
        } catch (UncaughtException x) {
            throw x;
        } catch (UninterpretableException x) {
            throw x;
        } catch (EarlyStopException x) {
            throw x;
        } catch (CascadedErrorException x) {
            throw x;
        } catch (JPF.ExitException x) {
            throw x;
        } catch (Throwable t) {
            throw new JPFListenerException("exception during methodExited() notification", t);
        }
    }

    protected void notifyExecuteInstruction(ThreadInfo ti, Instruction insn) {
        try {
            for (int i = 0; i < listeners.length; i++) {
                listeners[i].executeInstruction(this, ti, insn);
            }
        } catch (UncaughtException x) {
            throw x;
        } catch (UninterpretableException x) {
            throw x;
        } catch (EarlyStopException x) {
            throw x;
        } catch (CascadedErrorException x) {
            throw x;
        } catch (JPF.ExitException x) {
            throw x;
        } catch (Throwable t) {
            throw new JPFListenerException(
                    "exception during executeInstruction() notification", t);
        }
    }

    protected void notifyInstructionExecuted(ThreadInfo ti, Instruction insn,
            Instruction nextInsn) {
        try {
            // listener.instructionExecuted(this);
            for (int i = 0; i < listeners.length; i++) {
                listeners[i].instructionExecuted(this, ti, nextInsn, insn);
            }
        } catch (UncaughtException x) {
            throw x;
        } catch (UninterpretableException x) {
            throw x;
        } catch (EarlyStopException x) {
            throw x;
        } catch (CascadedErrorException x) {
            throw x;
        } catch (JPF.ExitException x) {
            throw x;
        } catch (Throwable t) {
            throw new JPFListenerException(
                    "exception during instructionExecuted() notification", t);
        }
    }

    public Instruction handleException(ThreadInfo ti, int exceptionObjRef) {
        ElementInfo ei = ti.getElementInfo(exceptionObjRef);
        ClassInfo ci = ei.getClassInfo();

        if (Ares.PrintStackTrace) {
            ti.printStackTrace(exceptionObjRef);
        }
        
        if (ci.isInstanceOf("java.lang.RuntimeException")) {
            checkExceptionalStopping(ti, ci);
        }

        return null;
    }

    protected boolean isUninterpretable(ClassInfo ci) {
        String cname = ci.getName();
        return isUninterpretable(cname);
    }

    public static boolean isUninterpretable(String cname) {
        return cname.equals("java.lang.UnsatisfiedLinkError")
                || cname.equals("java.lang.NoSuchMethodError")
                || cname.equals("java.lang.NoSuchFieldError")
                || cname.equals("java.lang.ClassNotFoundException");
    }

    public static boolean isTrivial(String cname) {
        return cname.equals("java.lang.Exception") 
                || cname.equals("java.lang.Throwable");
    }

    /**
     * an exception is thrown during JPF execution
     * @param ti
     * @param ci
     */
    protected void checkExceptionalStopping(ThreadInfo ti, ClassInfo ci) {
        StackFrame frame = RecoveryHelper.handlerFrameFor(ti, ci, null, true, true); 
        if (frame == null) {
            if (isUninterpretable(ci)) {
                throw new UninterpretableException("Uninterpretable behavior caused by " + ci.getName());
            }

            CascadedErrorException e = new CascadedErrorException("Cascaded exception " + ci.getName());
            e.setExceptionClassInfo(ci);
            throw e;
        }
    }

    public void printActions() {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        this.recoveryStrategy.printActions();
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    }
}
