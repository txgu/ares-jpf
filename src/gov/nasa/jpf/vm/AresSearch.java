package gov.nasa.jpf.vm;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.search.Search;

public class AresSearch extends Search {

    AresVM aresVM;

    public AresSearch(Config config, VM vm) {
        super(config, vm);
        aresVM = (AresVM) vm;
    }

    @Override
    public void search() {
        ChoiceGenerator<?> rootCg = aresVM.getNextChoiceGenerator();
        while(aresVM.hasNextRecoveryAction()){
            try {
                aresVM.recoverAndForward();
            } finally {
                aresVM.backtrack();
                rootCg.reset();
                aresVM.ss.curCg = null;
                aresVM.ss.nextCg = null;
                aresVM.setMandatoryNextChoiceGenerator(rootCg, "Cannot reset rootCg");
            }

        }

    }
}
