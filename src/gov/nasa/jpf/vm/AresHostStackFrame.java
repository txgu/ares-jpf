package gov.nasa.jpf.vm;

/**
 * This stack frame is in the recovery context and created from the host frame.
 * @author t
 *
 */
public interface AresHostStackFrame {
    boolean isExecutable();
    void setExecutable(boolean value);
}
