package gov.nasa.jpf.jvm;

import gov.nasa.jpf.vm.BootstrapMethodInfo;
import gov.nasa.jpf.vm.ClassFileContainer;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ClassLoaderInfo;
import gov.nasa.jpf.vm.ClassParseException;
import gov.nasa.jpf.vm.AresHostStackFrame64;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.NativeMethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Types;

public class AresClassInfo extends JVMClassInfo {

    /**
     * 
     */
    public AresClassInfo(ClassInfo funcInterface,
            BootstrapMethodInfo bootstrapMethod, String name,
            String samUniqueName, String[] fieldTypesName) {
        super(funcInterface, bootstrapMethod, name, samUniqueName, fieldTypesName);
    }

    public AresClassInfo(ClassInfo ciAnnotation, String proxyName,
            ClassLoaderInfo cli, String url) {
        super(ciAnnotation, proxyName, cli, url);
    }

    public AresClassInfo(String name, ClassLoaderInfo cli,
            ClassFile cf, String srcUrl, JVMCodeBuilder cb)
                    throws ClassParseException {
        super(name, cli, cf, srcUrl, cb);
    }

    public void setContainer(ClassFileContainer container) {
        this.container = container;
    }

    public StackFrame createStackFrame (ThreadInfo ti, MethodInfo callee){

        if (callee.isMJI()){
            NativeMethodInfo nativeCallee = (NativeMethodInfo) callee;
            JVMNativeStackFrame calleeFrame = new JVMNativeStackFrame( nativeCallee);
            calleeFrame.setArguments( ti);

            //			StackFrame callerFrame = ti.getTopFrame();
            //			System.out.println("callerFrame" + callerFrame);
            //			System.out.println("calleeFrame" + calleeFrame);
            return calleeFrame; 

        } else {
            JVMStackFrame calleeFrame = new JVMStackFrame( callee);

            StackFrame callerFrame = ti.getTopFrame();

            if (callerFrame instanceof AresHostStackFrame64) {
                setCallArguments64(ti, callerFrame, calleeFrame);
            } else {
                calleeFrame.setCallArguments( ti);
            }


            return calleeFrame;
        }
    }

    private void setCallArguments64(ThreadInfo ti, StackFrame callerFrame,
            JVMStackFrame calleeFrame) {
        MethodInfo miCallee = calleeFrame.getMethodInfo();
        int argSize = miCallee.getArgumentsSize();

        if (argSize > 0) {
            byte[] argTypes = miCallee.getArgumentTypes();

            int nArgs = argTypes.length;

            int i, j, k, stackOffset;

            for (i = 0, stackOffset = 0, j = argSize - 1, k = nArgs - 1;
                    i < nArgs;
                    i++, j--, k--
                    ) {
                switch (argTypes[k]) {
                case Types.T_BOOLEAN:
                case Types.T_BYTE:
                case Types.T_CHAR:
                case Types.T_SHORT:
                case Types.T_INT:
                case Types.T_FLOAT:
                    calleeFrame.setLocalAttr(j, callerFrame.getOperandAttr(stackOffset));
                    calleeFrame.setLocalVariable(j, callerFrame.peek(stackOffset));
                    break;
                case Types.T_LONG:
                case Types.T_DOUBLE:
                    calleeFrame.setLocalAttr(j, callerFrame.getOperandAttr(stackOffset));
                    j--;
                    stackOffset++; // 2 stack words
                    calleeFrame.setLocalAttr(j, callerFrame.getOperandAttr(stackOffset));
                    calleeFrame.setLongLocalVariable(j, callerFrame.peekLong(stackOffset));
                    break;
                default:
                    // NOTE - we have to store T_REFERENCE as an Integer, because
                    // it shows up in our native method as an 'int'
                    calleeFrame.setLocalVariable(j, callerFrame.peek(stackOffset), true);
                }

                stackOffset++;
            }



            if (!miCallee.isStatic()) {

                calleeFrame.setLocalVariable(j, callerFrame.peek(stackOffset), true);

                int ref = callerFrame.getCalleeThis(miCallee);
                if (ti.getElementInfo(ref) == null) {
                    throw new RuntimeException("This should not be null");
                }
                calleeFrame.setThis(ref);
            }
        }
    }



}