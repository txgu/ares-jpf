package gov.nasa.jpf.jvm;

import gov.nasa.jpf.vm.ClassFileMatch;
import gov.nasa.jpf.vm.ClassParseException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class AresSystemClassLoaderFileContainer extends AresClassLoaderFileContainer {

    public AresSystemClassLoaderFileContainer(String name, String url) {
        super(name, url, ClassLoader.getSystemClassLoader());
    }

    @Override
    public ClassFileMatch getMatch(String clsName) throws ClassParseException {
        String rn = clsName.replace('.', '/') + ".class";

        URL url = hostClassLoader.getResource(rn);
        if (url == null) {
            return null;
        }
        byte[] data = null;
        try(InputStream input = url.openStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream(1024*2)){
            byte[] buf = new byte[1024];
            int pos = -1;
            while((pos = input.read(buf))!= -1){
                baos.write(buf,0,pos);
            }
            baos.flush(); //..
            data = baos.toByteArray();
        } catch (IOException e) {
            throw new ClassParseException("Error in reading stream", e);
        }


        return new AresClassFileMatch(clsName, url.toString(), data);
    }
}
