package gov.nasa.jpf.jvm;

public interface DelegateClassLoaderInfo {
    ClassLoader getDelegationClassLoader();
}
