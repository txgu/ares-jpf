package gov.nasa.jpf.jvm;

import gov.nasa.jpf.vm.ApplicationContext;
import gov.nasa.jpf.vm.DynamicElementInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StaticElementInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

public class AresScheduler implements gov.nasa.jpf.vm.Scheduler {

    @Override
    public void initializeSyncPolicy(VM vm, ApplicationContext appCtx) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void initializeThreadSync(ThreadInfo tiCurrent, ThreadInfo tiNew) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setRootCG() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean setsBlockedThreadCG(ThreadInfo ti, ElementInfo ei) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsLockAcquisitionCG(ThreadInfo ti, ElementInfo ei) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsLockReleaseCG(ThreadInfo ti, ElementInfo ei,
            boolean didUnblock) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsTerminationCG(ThreadInfo ti) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsWaitCG(ThreadInfo ti, long timeout) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsNotifyCG(ThreadInfo ti, boolean didNotify) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsNotifyAllCG(ThreadInfo ti, boolean didNotify) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsStartCG(ThreadInfo tiCurrent, ThreadInfo tiStarted) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsYieldCG(ThreadInfo ti) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsPriorityCG(ThreadInfo ti) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsSleepCG(ThreadInfo ti, long millis, int nanos) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsSuspendCG(ThreadInfo tiCurrent, ThreadInfo tiSuspended) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsResumeCG(ThreadInfo tiCurrent, ThreadInfo tiResumed) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsJoinCG(ThreadInfo tiCurrent, ThreadInfo tiJoin,
            long timeout) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsStopCG(ThreadInfo tiCurrent, ThreadInfo tiStopped) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsInterruptCG(ThreadInfo tiCurrent,
            ThreadInfo tiInterrupted) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsParkCG(ThreadInfo ti, boolean isAbsTime, long timeout) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsUnparkCG(ThreadInfo tiCurrent, ThreadInfo tiUnparked) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsBeginAtomicCG(ThreadInfo ti) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsEndAtomicCG(ThreadInfo ti) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsRescheduleCG(ThreadInfo ti, String reason) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsPostFinalizeCG(ThreadInfo tiFinalizer) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void initializeSharednessPolicy(VM vm, ApplicationContext appCtx) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void initializeObjectSharedness(ThreadInfo allocThread,
            DynamicElementInfo ei) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void initializeClassSharedness(ThreadInfo allocThread,
            StaticElementInfo ei) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean canHaveSharedObjectCG(ThreadInfo ti, Instruction insn,
            ElementInfo eiFieldOwner, FieldInfo fi) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ElementInfo updateObjectSharedness(ThreadInfo ti, ElementInfo ei,
            FieldInfo fi) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean setsSharedObjectCG(ThreadInfo ti, Instruction insn,
            ElementInfo eiFieldOwner, FieldInfo fi) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean canHaveSharedClassCG(ThreadInfo ti, Instruction insn,
            ElementInfo eiFieldOwner, FieldInfo fi) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ElementInfo updateClassSharedness(ThreadInfo ti, ElementInfo ei,
            FieldInfo fi) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean setsSharedClassCG(ThreadInfo ti, Instruction insn,
            ElementInfo eiFieldOwner, FieldInfo fi) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean canHaveSharedArrayCG(ThreadInfo ti, Instruction insn,
            ElementInfo eiArray, int idx) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ElementInfo updateArraySharedness(ThreadInfo ti,
            ElementInfo eiArray, int idx) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean setsSharedArrayCG(ThreadInfo ti, Instruction insn,
            ElementInfo eiArray, int idx) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsSharedObjectExposureCG(ThreadInfo ti, Instruction insn,
            ElementInfo eiFieldOwner, FieldInfo fi, ElementInfo eiExposed) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean setsSharedClassExposureCG(ThreadInfo ti, Instruction insn,
            ElementInfo eiFieldOwner, FieldInfo fi, ElementInfo eiExposed) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void cleanupThreadTermination(ThreadInfo ti) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void initialize(VM vm, ApplicationContext appCtx) {
        // TODO Auto-generated method stub
        
    }

}
