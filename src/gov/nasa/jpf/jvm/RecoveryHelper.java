package gov.nasa.jpf.jvm;

import gov.nasa.jpf.Ares;
import gov.nasa.jpf.vm.ClassChangeException;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ClassLoaderInfo;
import gov.nasa.jpf.vm.ExceptionHandler;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.AresVM;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.bytecode.InvokeInstruction;

public class RecoveryHelper {

    public static void pushDefaultValue(StackFrame frame, String type){
        if (type.equals("I")
                || type.equals("C")
                || type.equals("Z")
                || type.equals("B")
                || type.equals("S")) {
            frame.push(0);
        } else if(type.equals("J")) {
            frame.pushLong(0L);
        } else if (type.equals("D")) {
            frame.pushDouble(0D);
        } else if (type.equals("F")) {
            frame.pushFloat(0F);
        } else if (type.equals("V")) {

        } else {
            frame.pushRef(MJIEnv.NULL);
        }
    }

    public static MethodInfo getStaticTarget(MethodInfo from, String cname, String mname) {
        ClassLoaderInfo cl = from.getClassInfo().getClassLoaderInfo();
        ClassInfo ci = cl.getResolvedClassInfo(cname);
        boolean recursiveLookup = (mname.charAt(0) != '<'); // no hierarchy lookup for <init>
        MethodInfo mi = null;
        try {
            mi = ci.getMethod(mname, recursiveLookup); 
        } catch (ClassChangeException ccx){
            return null;
        }

        return mi;
    }

    public static StackFrame handlerFrameFor (ThreadInfo ti, ClassInfo ciException) {
        return handlerFrameFor(ti, ciException, null, Ares.IgnoreFinallyBlock, false);
    }

    public static StackFrame handlerFrameFor (ThreadInfo ti, ClassInfo ciException, StackFrame beginFrame) {
        return handlerFrameFor(ti, ciException, beginFrame, Ares.IgnoreFinallyBlock, false);
    }

    public static StackFrame handlerFrameFor (ThreadInfo ti, ClassInfo ciException, StackFrame beginFrame, boolean ignoreFinally, boolean ignoreTrivial){
        boolean hasHandler = false;
        if (beginFrame == null) {
            beginFrame = ti.getTopFrame();
        }
        for (StackFrame frame = beginFrame; frame != null; frame = frame.getPrevious()) {
            // that means we have to turn the exception into an InvocationTargetException
            if (frame.isReflection()) {
                ciException = ClassInfo.getInitializedSystemClassInfo("java.lang.reflect.InvocationTargetException", ti);
                throw new RuntimeException("Not implemented yet");
            }

            hasHandler = hasHandlerFor(ti, frame, ciException, ignoreFinally, ignoreTrivial);
            if (hasHandler){
                return frame;
            }
        }

        return null;
    }

    public static boolean hasHandlerFor(ThreadInfo ti, StackFrame frame, ClassInfo ciException, boolean ignoreFinally, boolean ignoreTrivial) {
        MethodInfo mi = frame.getMethodInfo();
        Instruction insn = frame.getPC();
        ExceptionHandler[] exceptionHandlers = mi.getExceptions();
        ClassInfo ci = mi.getClassInfo();
        if (exceptionHandlers != null) {
            int position = insn.getPosition();
            for (int i=0; i<exceptionHandlers.length; i++){
                ExceptionHandler handler = exceptionHandlers[i];
                if ((position >= handler.getBegin()) && (position < handler.getEnd())) {
                    // checks if this type of exception is caught here (null means 'any')
                    String handledType = handler.getName();

                    if (handledType == null) {
                        if (ignoreFinally) {
                            continue;
                        }
                        return true;
                    }

                    ClassInfo ht = ci.getClassLoaderInfo().loadClass(handledType);

                    if (ignoreTrivial) {
                        if (AresVM.isTrivial(ht.getName())) {
                            continue;
                        }
                    }

                    if (ciException.isInstanceOf(ht)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public static MethodInfo getStaticCallee(ThreadInfo ti, StackFrame frame) {
        Instruction pc = frame.getPC();

        if (pc instanceof InvokeInstruction) {
            InvokeInstruction ii = (InvokeInstruction) pc;
            String cname = ii.getInvokedMethodClassName();
            String mname = ii.getInvokedMethodName();

            ClassInfo ci = frame.getClassInfo().getClassLoaderInfo().loadClass(cname);
            return ci.getMethod(mname, true);
        }

        return null;
    }


}
