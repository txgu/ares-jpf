package gov.nasa.jpf.jvm;

import gov.nasa.jpf.JPFException;
import gov.nasa.jpf.vm.ClassFileMatch;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ClassLoaderInfo;
import gov.nasa.jpf.vm.ClassParseException;
import gov.nasa.jpf.vm.ClassPath;
import gov.nasa.jpf.vm.AresVM;
import gov.nasa.jpf.vm.VM;

public class AresClassLoaderInfo extends ClassLoaderInfo implements DelegateClassLoaderInfo {

    protected AresVM aresVM;
    protected AresClassLoaderFileContainer fileContainer;

    public AresClassLoaderInfo (VM vm, int objRef, ClassLoaderInfo parent, ClassLoader cl) {
        super(vm, objRef, new ClassPath(), parent);

        System.out.println("************ Create a custom class loader! ***************");
        System.out.println(cl);
        System.out.println("**********************************************************");

        aresVM = (AresVM) vm;
        fileContainer = new AresClassLoaderFileContainer("@","@",cl);
    }


    /**
     * TODO: we use parent-first delegation model to load the class.
     * Hope it works for all subjects.. 
     */
    public ClassInfo loadClass(String cname) {
        //System.out.println("Loading " + cname + " from " + fileContainer.getDelegationClassLoader());
        return loadClassOnJVM(cname);
    }

    protected ClassFileMatch getMatch(String typeName) {
        if(ClassInfo.isBuiltinClass(typeName)) {
            return null;
        }

        ClassFileMatch match;

        try {
            match = fileContainer.getMatch(typeName);
        } catch (ClassParseException cfx){
            throw new JPFException("error reading class " + typeName, cfx);
        }

        return match;
    }

    @Override
    public ClassLoader getDelegationClassLoader() {
        return fileContainer.getDelegationClassLoader();
    }

    public String toString() {
        return "Delegation for " + fileContainer.getDelegationClassLoader();
    }

}
