package gov.nasa.jpf.jvm;

import gov.nasa.jpf.vm.AnnotationInfo;
import gov.nasa.jpf.vm.ClassFileContainer;
import gov.nasa.jpf.vm.ClassFileMatch;
import gov.nasa.jpf.vm.ClassLoaderInfo;
import gov.nasa.jpf.vm.ClassParseException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class AresClassLoaderFileContainer extends JVMClassFileContainer {

    static URLClassLoader extraClassFileContainer;

    static {
        String extraClassPath = System.getProperty("org.javelus.ares.extraClassPath");
        if (extraClassPath != null) {
            extraClassFileContainer = new URLClassLoader(pathToURLs(extraClassPath));
        }
    }

    protected ClassLoader hostClassLoader;

    public AresClassLoaderFileContainer(String name, String url, ClassLoader hostClassLoader) {
        super(name, url);
        this.hostClassLoader = hostClassLoader;
    }

    private static URL[] pathToURLs(String extraClassPath) {
        List<URL> urls = new ArrayList<URL>();
        String[] paths = extraClassPath.split(File.pathSeparator);
        for (String path:paths) {
            path = path.trim();
            if (path.isEmpty()) {
                continue;
            }
            try {
                urls.add(Paths.get(path).toUri().toURL());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return urls.toArray(new URL[urls.size()]);
    }

    @Override
    public ClassFileMatch getMatch(String clsName) throws ClassParseException {
        String rn = clsName.replace('.', '/') + ".class";

        URL url = hostClassLoader.getResource(rn); //HeapAdapter.findJavaResource(hostClassLoader, rn);

        if (url == null && extraClassFileContainer != null) {
            url = extraClassFileContainer.getResource(rn);
        }

        if (url == null) {
            return null;
        }

        byte[] data = null;
        try(InputStream input = url.openStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream(1024*2)){
            byte[] buf = new byte[1024];
            int pos = -1;
            while((pos = input.read(buf))!= -1){
                baos.write(buf,0,pos);
            }
            baos.flush(); //..
            data = baos.toByteArray();
        } catch (IOException e) {
            throw new ClassParseException("Error in reading stream", e);
        }


        return new AresClassFileMatch(clsName, url.toString(), data);
    }

    public class AresClassFileMatch extends JVMClassFileMatch {

        AresClassFileMatch (String typeName, String url, byte[] data) {
            super(typeName, url, data);
        }

        @Override
        public ClassFileContainer getContainer(){
            return AresClassLoaderFileContainer.this;
        }


        @Override
        public JVMClassInfo createClassInfo (ClassLoaderInfo loader) throws ClassParseException {
            JVMSystemClassLoaderInfo sysCli = (JVMSystemClassLoaderInfo)loader.getSystemClassLoader();

            JVMCodeBuilder cb = sysCli.getCodeBuilder(typeName);
            ClassFile cf = new ClassFile(data);

            AresClassInfo ci = new AresClassInfo(typeName, loader, cf, url, cb);

            ci.setContainer(AresClassLoaderFileContainer.this);

            return ci;
        }

        @Override
        public AnnotationInfo createAnnotationInfo (ClassLoaderInfo loader) throws ClassParseException {
            ClassFile cf = new ClassFile(data);
            JVMAnnotationParser parser = new JVMAnnotationParser(cf);

            return new AnnotationInfo(typeName, loader, parser);
        }
    }

    public ClassLoader getDelegationClassLoader() {
        return hostClassLoader;
    }
}
