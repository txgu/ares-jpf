package gov.nasa.jpf.jvm;

import gov.nasa.jpf.jvm.JVMClassFileContainer.JVMClassFileMatch;
import gov.nasa.jpf.vm.ClassLoaderInfo;
import gov.nasa.jpf.vm.ClassParseException;

public class JPFClassFileMatchWrapper extends JVMClassFileMatch {

    public JPFClassFileMatchWrapper(JVMClassFileContainer jvmClassFileContainer,
            JVMClassFileMatch origin) {
        jvmClassFileContainer.super(origin.typeName, origin.url, origin.data);
    }

    @Override
    public JVMClassInfo createClassInfo (ClassLoaderInfo loader) throws ClassParseException {
        JVMSystemClassLoaderInfo sysCli = (JVMSystemClassLoaderInfo)loader.getSystemClassLoader();

        JVMCodeBuilder cb = sysCli.getCodeBuilder(typeName);
        ClassFile cf = new ClassFile(data);

        AresClassInfo ci = new AresClassInfo(typeName, loader, cf, url, cb);

        return ci;
    }

}
