package gov.nasa.jpf.jvm;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPFException;
import gov.nasa.jpf.jvm.JVMClassFileContainer.JVMClassFileMatch;
import gov.nasa.jpf.vm.ClassFileMatch;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ClassParseException;
import gov.nasa.jpf.vm.AresVM;
import gov.nasa.jpf.vm.VM;

import java.io.File;

public class AresSystemClassLoaderInfo extends JVMSystemClassLoaderInfo implements DelegateClassLoaderInfo {

    AresVM aresVM;
    AresSystemClassLoaderFileContainer fileContainer;

    public AresSystemClassLoaderInfo(VM vm, int appId) {
        super(vm, appId);

        aresVM = (AresVM) vm;
        fileContainer = new AresSystemClassLoaderFileContainer("#","#");
    }

    protected ClassFileMatch getMatch(String typeName) {
        if(ClassInfo.isBuiltinClass(typeName)) {
            return null;
        }

        ClassFileMatch match = super.getMatch(typeName);

        if (match != null) {
            return new JPFClassFileMatchWrapper(fileContainer, (JVMClassFileMatch) match);
        }

        try {
            match = fileContainer.getMatch(typeName);
        } catch (ClassParseException cfx){
            throw new JPFException("error reading class " + typeName, cfx);
        }

        return match;
    }

    @Override
    protected void initializeSystemClassPath (VM vm, int appId) {
        Config conf = vm.getConfig();
        File[] pathElements;

        pathElements = getPathElements(conf, "classpath", appId);
        if (pathElements != null) {
            for (File f : pathElements) {
                addClassPathElement(f.getAbsolutePath());
            }
        }

        log.info("collected system classpath: ", cp);
    }

    @Override
    public ClassLoader getDelegationClassLoader() {
        return fileContainer.getDelegationClassLoader();
    }

}
