package gov.nasa.jpf.listener;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

public class FullExecTracker extends ExecTracker {

    public FullExecTracker(Config config) {
        super(config);
    }

    @Override
    public void executeInstruction(VM vm, ThreadInfo currentThread,
            Instruction instructionToExecute) {
        out.printf("Pre-[%04x]@%d   ", instructionToExecute.getPosition(), currentThread.getTopFrame().getDepth());
        out.println(instructionToExecute.toString());
    }

    public void instructionExecuted(VM vm, ThreadInfo ti, Instruction nextInsn, Instruction executedInsn) {
        super.instructionExecuted(vm, ti, nextInsn, executedInsn);
    }
}
