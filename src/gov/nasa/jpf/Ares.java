package gov.nasa.jpf;

import gov.nasa.jpf.jvm.AresSystemClassLoaderInfo;
import gov.nasa.jpf.listener.ExecTracker;
import gov.nasa.jpf.listener.FullExecTracker;
import gov.nasa.jpf.listener.MethodTracker;
import gov.nasa.jpf.util.RunRegistry;
import gov.nasa.jpf.vm.AresSearch;
import gov.nasa.jpf.vm.AresVM;
import gov.nasa.jpf.vm.NoOutOfMemoryErrorProperty;

import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.javelus.ares.HeapAdapter;

public class Ares extends JPF {

    public static final boolean IgnoreFinallyBlock = true;

    public static final boolean debugStackFrame = false;

    public static final boolean DebugFailureDesc;

    public static final boolean debugAresVM = false;

    public static final boolean debugHeapAdapter = false;

    public static final boolean debugRecoveryStrategy = false;

    public static final boolean debugStackBuilder = false;

    public static final boolean printActions = true;

    public static final boolean printTime = false;

    public static final boolean debug = false;

    public static final boolean PrintStackTrace;

    public static final boolean PrintResults;

    public static final boolean PrintInternalError = true;

    public static final int MaxEarlyReturnCount;

    public static final int MaxStep;

    public static final boolean TraceMethod;

    public static final boolean TraceInstruction;

    public static final boolean IncludeCascadedError;

    public static final int AcceptThreshold;
    
    AresVM aresVM;
    AresSearch aresSearch;

    private static Ares instance;

    public static Ares v() {
        return instance;
    }

    static {
        AcceptThreshold = Integer.getInteger("org.javelus.ares.acceptThreshold", 100);

        PrintStackTrace = Boolean.getBoolean("org.javelus.ares.printStackTrace");
        PrintResults = Boolean.parseBoolean(System.getProperty("org.javelus.ares.printResults", "true"));
        DebugFailureDesc = Boolean.getBoolean("org.javelus.ares.debugFailureDesc");

        IncludeCascadedError = Boolean.getBoolean("org.javelus.ares.includeCascadedError");
        MaxEarlyReturnCount = Integer.getInteger("org.javelus.ares.maxEarlyReturnCount", 5);
        MaxStep = Integer.getInteger("org.javelus.ares.timeoutSteps", 1000);;

        // default is false
        TraceMethod = Boolean.getBoolean("org.javelus.ares.traceMethod");
        TraceInstruction = Boolean.getBoolean("org.javelus.ares.traceInstruction");

        try {
            // Create an empty config
            Config config = new Config(new String[0]);

            // load the jpf.properties copied from jpf-core
            URL jpfCoreProperties = Ares.class.getResource("jpf.properties");
            config.loadProperties(jpfCoreProperties);

            if (jpfCoreProperties.getProtocol().equals("file")) {
                // we are in a testing mode in eclipse, i.e., ares-jpf/bin/
                // TODO, now we should import the jpf-core project in eclipse
            } else if (jpfCoreProperties.getProtocol().equals("jar")) {
                // we are in deployed mode, a jar file
                String path = jpfCoreProperties.getPath();
                if (!path.startsWith("file:")) {
                    throw new RuntimeException("Malformed URL for a JAR file, " + jpfCoreProperties);
                }
                int end = path.indexOf("/ares-jpf.jar!");
                if (end < 5) {
                    throw new RuntimeException("gov.nasa.jpf.Ares should be packed into ares-jpf.jar");
                }
                path = path.substring(5, end);
                File aresJPFHome = new File(path);
                File classesJar = new File(aresJPFHome, "lib/jpf-classes.jar");
                if (!classesJar.exists()) {
                    throw new RuntimeException("jpf-classes.jar does not exist at " + classesJar);
                }
                config.setProperty("classpath", classesJar.getAbsolutePath());
            }

            // Override custom properties
            // All native classes in ares-jpf should be loaded by the bootstrap classloader,
            // as an exception may happen in any class.
            config.setProperty("vm.class", AresVM.class.getName());
            config.setProperty("search.class", AresSearch.class.getName());
            config.setProperty("vm.classloader.class", AresSystemClassLoaderInfo.class.getName());
            config.setProperty("jvm.insn_factory.class", org.javelus.ares.jvm.bytecode.InstructionFactory.class.getName());
            //config.setProperty("vm.scheduler.class", AresScheduler.class.getName());


            config.setProperty("et.print_insn", "true");
            config.setProperty("et.print_src", "false");
            config.setProperty("et.print_mth", "false");
            config.setProperty("et.skip_init", "false");
            config.setProperty("et.show_shared", "false");

            instance = new Ares(config);
            instance.aresVM.runSystemClassClinits();

            if (TraceInstruction) {
                ExecTracker tracker = new FullExecTracker(config);
                instance.addListener(tracker);
            }

            if (TraceMethod) {
                MethodTracker mt = new MethodTracker(config, instance);
                instance.addListener(mt);
            }
        } catch (RuntimeException e) {
            System.err.println("Fail to bootstrap Ares-JPF");
            e.printStackTrace();
            throw e;
        }
    }

    private Ares(Config config) {
        super(config);

        if(!vm.initialize()){
            throw new RuntimeException("Fail to initialize VM");
        }

        aresVM = (AresVM) vm;
        aresSearch = (AresSearch) search;
    }



    public static HeapAdapter getHeapAdapter() {
        return v().getAresVM().getHeapAdapter();
    }

    public AresVM getAresVM() {
        return aresVM;
    }

    public Object[] run(Object[] data) {
        if (data == null || data.length == 0) {
            return null;
        }

        Runtime rt = Runtime.getRuntime();

        // this might be executed consecutively, so notify everybody
        RunRegistry.getDefaultRegistry().reset();

        if (isRunnable()) {
            try {
                try {
                    aresVM.buildFailureThread(data);
                    aresVM.buildRecoveryStrategy();
                } catch (Throwable e) {
                    if (debug) {
                        System.out.println("Preparing recovery failed with exception " + e);
                        if (PrintStackTrace) {
                            e.printStackTrace();
                        }
                    }
                    throw e;
                }

                if (printActions) {
                    aresVM.printActions();
                }

                status = Status.RUNNING;
                search.search();

                if (debug) {
                    System.out.println("UseJPF: Final recovery action is " + aresVM.getFinalRecoveryAction());
                }

                if (PrintResults) {
                    aresVM.printResults();
                }

                return aresVM.getEncodedFinalRecoveryAction();
            } catch (OutOfMemoryError oom) {

                // try to get memory back before we do anything that makes it worse
                // (note that we even try to avoid calls here, we are on thin ice)

                // NOTE - we don't try to recover at this point (that is what we do
                // if we fall below search.min_free within search()), we only want to
                // terminate gracefully (incl. report)

                memoryReserve = null; // release something
                long m0 = rt.freeMemory();
                long d = 10000;

                // see if we can reclaim some memory before logging or printing statistics
                for (int i=0; i<10; i++) {
                    rt.gc();
                    long m1 = rt.freeMemory();
                    if ((m1 <= m0) || ((m1 - m0) < d)) {
                        break;
                    }
                    m0 = m1;
                }

                logger.severe("JPF out of memory");

                // that's questionable, but we might want to see statistics / coverage
                // to know how far we got. We don't inform any other listeners though
                // if it throws an exception we bail - we can't handle it w/o memory
                try {
                    search.notifySearchConstraintHit("JPF out of memory");
                    search.error(new NoOutOfMemoryErrorProperty());            // JUnit tests will succeed if OOM isn't flagged.
                    reporter.searchFinished(search);
                } catch (Throwable t){
                    throw new JPFListenerException("exception during out-of-memory termination", t);
                }

                // NOTE - this is not an exception firewall anymore

            } finally {
                status = Status.DONE;

                aresVM.resetRecoveryThread();

                config.jpfRunTerminated();
                cleanUp();
            }
        }

        return null;
    }

    public static synchronized Object[] runDefault(Object[] data) {
        long beginTime = System.nanoTime();
        try{
            return v().run(data);
        } finally {
            long delta = System.nanoTime() - beginTime;
            if (printTime) {
                System.out.format("(%d)ns(%d)ms", delta, TimeUnit.NANOSECONDS.toMillis(delta));
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("Hello world!");
    }
}
