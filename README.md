# Embed JPF in Host VM




```bash
ARES_LOG="-XX:TraceRuntimeRecovery=2060"

export ARES_HOME=/code/ares/hotspot/
export ARES_JPF_HOME=/code/ares/ares-jpf
export ARES_BIN=${ARES_HOME}/build/linux/linux_amd64_compiler2/fastdebug/hotspot


# JPF Options
ARES_JPF="${ARES_JPF_HOME}/ares-jpf.jar:${ARES_JPF_HOME}/lib/jpf-classes.jar:${ARES_JPF_HOME}/lib/jpf.jar"

# Use JPF
export JPF="$ARES_LOG -XX:+UseJPF -Xbootclasspath/a:${ARES_JPF}"

# Use 1-ER
export FER="$ARES_LOG -XX:-UseErrorTransformation -XX:+UseEarlyReturn -XX:-OnlyEarlyReturnVoid"

# Use VOER
export VOER="$ARES_LOG -XX:-UseErrorTransformation -XX:+UseEarlyReturn -XX:+OnlyEarlyReturnVoid"

# Use SBET
export SBET="$ARES_LOG -XX:+UseErrorTransformation -XX:-UseEarlyReturn -XX:+UseStack -XX:-UseForceThrowable"

# Use FTET
export FTET="$ARES_LOG -XX:+UseErrorTransformation -XX:-UseEarlyReturn -XX:-UseStack -XX:+UseForceThrowable"

# Used to invoke custom built jdk8
export ALT_JAVA_HOME=/code/jdk/jdk1.8.0_65/
```
